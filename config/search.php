<?php

return [
    'stop_words' => [
        'en' => [
            'for',
            'best',
            'about',
            'apps',
            'software',
        ],
    ],
];