<?php

return [
    'rules' => [
        'post' => [
            'title' => 'required',
            'title_slug' => 'required',
            'content' => 'required',
            'featured_image' => 'required',
            'locale' => 'required',
        ],
        'category' => [
            'name' => 'required',
            'name_slug' => 'required',
            'locale' => 'required',
        ],
    ]
];