<?php

return [
    'locale' => [
        'en' => env('URL_HOME_EN'),
        'es' => env('URL_HOME_ES'),
    ],
];
