<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('title')->nullable();
            $table->string('url')->nullable();
            $table->boolean('is_featured')->default(0);
            $table->string('title_slug')->nullable();
            $table->unsignedSmallInteger('position');
            $table->string('description', 255)->nullable();
            $table->boolean('visible')->default(true);
            $table->unsignedInteger('gallery_id');
            $table->string('source_name')->nullable();
            $table->string('source_link', 255)->nullable();

            $table->string('meta_description', 255)->nullable();
            $table->string('meta_title', 255)->nullable();
            $table->string('meta_keywords', 255)->nullable();

            $table->timestamps();

            $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
