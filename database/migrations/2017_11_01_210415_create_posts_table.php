<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('locale', 2);
            $table->string('title');
            $table->string('title_slug');
            $table->string('excerpt', 255)->nullable();
            $table->string('featured_image');
            $table->text('content');
            $table->boolean('visible')->default(true);

            $table->string('meta_description', 255)->nullable();
            $table->string('meta_title', 255)->nullable();
            $table->string('meta_keywords', 255)->nullable();

            $table->unsignedInteger('category_id')->nullable();

            $table->timestamps();

            $table->unique(['title_slug', 'locale']);
        });

        \Illuminate\Support\Facades\DB::statement('ALTER TABLE posts ADD FULLTEXT full(title, excerpt)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE posts DROP INDEX full');
        Schema::dropIfExists('posts');
    }
}
