<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Category::class, function (Faker $faker) {
    $name = $faker->sentence;

    return [
        'locale' => $faker->randomElements(['es', 'en'])[0],
        'name' => $name,
        'name_slug' => str_slug($name),
        'icon' => null,
        'parent_id' => null,
    ];
});
