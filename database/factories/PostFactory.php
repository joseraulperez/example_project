<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Post::class, function (Faker $faker) {
    $title = $faker->sentence;

    return [
        'locale' => $faker->randomElements(['es', 'en'])[0],
        'title' => $faker->sentence,
        'title_slug' => str_slug($title),
        'excerpt' => $faker->sentence,
        'featured_image' => $faker->image(null, 840, 420),
        'content' => $faker->paragraphs(10, true),
        'visible' => $faker->randomElements([0, 1])[0],
        'meta_description' => $faker->sentence,
        'meta_title' => $title,
        'meta_keywords' => null,
        'featured_image_source_name' => null,
        'featured_image_source_link' => null,
        'featured_image_description' => null,
        'category_id' => function() {
            return factory(App\Models\Category::class)->create()->id;
        },
    ];
});
