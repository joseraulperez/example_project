<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Http\Middleware\SetLocale;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Config;

class SetLocaleTest extends TestCase
{
    protected $set_locale_middleware;

    public function getUrlsProvider()
    {
        return [
            [

                'http://sub-domain.example.com',
                'sub-domain',
                'Existent sub-domain should set "sub-domain" as locale',
            ],
            [
                'http://www.example.com',
                'default',
                '"www" sub-domain should set "default" as locale',
            ],
            [
                'http://xxx.example.com',
                'default',
                'Unknown sub-domain should set "default" locale',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider getUrlsProvider
     *
     * @param $url
     * @param $expected
     * @param $message
     */
    public function different_sub_domains_set_different_locale($url, $expected, $message)
    {
        Config::set('sub_domains', [
            'available' => [
                'sub-domain' => true,
            ],
            'default' => 'default',
        ]);

        URL::shouldReceive('current')
            ->once()
            ->andReturn($url);

        $set_locale_middleware = new SetLocale();
        $set_locale_middleware->handle(new Request(), function () {});

        $this->assertEquals($expected, app()->getLocale(), $message);
    }
}
