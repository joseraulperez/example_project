<?php

namespace Tests\Feature\Main\Controllers;

use App\Models\Post;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class StaticControllerTest extends TestCase
{
    /** @test */
    public function when_user_visits_privacy_policy_the_view_is_privacy_policy()
    {
        $response = $this->get('/privacy-policy');
        $response->assertViewIs('main.static.privacy_policy');
    }

    /** @test */
    public function when_user_visits_terms_of_service_the_view_is_terms_of_service()
    {
        $response = $this->get('/terms-of-service');
        $response->assertViewIs('main.static.terms_of_service');
    }

    /** @test */
    public function when_user_visits_contact_us_the_view_is_contact_us()
    {
        $response = $this->get('/contact-us');
        $response->assertViewIs('main.static.contact_us');
    }
}
