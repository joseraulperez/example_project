<?php

namespace Tests\Feature\Main\Controllers;

use App\Models\Post;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function post_is_assigned_to_view()
    {
        $posts = factory(Post::class, 2)->create([
            'visible' => 1,
            'locale' => 'en',
        ]);
        $response = $this->get('/');

        $response->assertDontSee($posts[0]->title);
        $response->assertSee($posts[1]->title);
    }
}
