<?php

namespace Tests\Feature\Admin\Controllers\Posts;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Config;
use Mockery;
use Tests\TestCase;

class EditControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Log in a user with a specific role.
     *
     * @param  $role
     */
    protected function logInUser($role)
    {
        $user = factory(User::class)->create([
            'role' => $role,
        ]);
        $this->be($user);
    }

    /** @test */
    public function a_post_categories_and_locales_are_assigned_to_view()
    {
        $this->logInUser(User::ROLE_ADMIN);

        $categories = factory(Category::class, 2)->create();
        $post = factory(Post::class)->create();
        Config::set('locales', ['locale' => 'fake-locale']);

        $response = $this->get('/admin/posts/' . $post->id);

        $response->assertSee($categories[0]->name);
        $response->assertSee($categories[1]->name);
        $response->assertSee('fake-locale');
        $response->assertSee($post->title);
    }

    /** @test */
    public function validation_fails_when_updating_a_post_if_rules_are_not_satisfied()
    {
        $this->logInUser(User::ROLE_ADMIN);

        Config::set('forms.rules.post', [
            'item' => 'required',
        ]);
        $post = factory(Post::class)->create();

        $response = $this->post('/admin/posts/' . $post->id, [
            'wrong_item' => 'fake-data',
        ]);

        $response->assertSessionHasErrors(['item']);
    }

    /** @test */
    public function when_all_data_is_passed_a_post_is_updated_in_the_database()
    {
        $this->logInUser(User::ROLE_ADMIN);
        $post = factory(Post::class)->create();

        $post->title = 'Fake title updated';

        $response = $this->post('/admin/posts/' . $post->id, $post->toArray());

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'title' => $post->title,
        ]);
        $response->assertRedirect('/admin/posts/' . $post->id);
    }

    /** @test */
    public function if_the_database_fails_redirects_with_the_error_message()
    {
        $this->logInUser(User::ROLE_ADMIN);

        $post = factory(Post::class)->create();
        Config::set('forms.rules.post', [
            'item' => 'required',
        ]);

        $post_mock = Mockery::mock(Post::class);
        $post_mock
            ->shouldReceive('fill')
            ->once()
            ->shouldReceive('save')
            ->once()
            ->andThrow(new \Exception('Fake error message'));

        $post_model_mock = Mockery::mock(Post::class);
        $post_model_mock
            ->shouldReceive('findOrFail')
            ->andReturn($post_mock)
            ->once();
        $this->app->instance(Post::class, $post_model_mock);

        $response = $this->post('/admin/posts/' . $post->id, ['item' => 'fake-data']);

        $response->assertSessionHas('message', [
            'level' => 'danger',
            'text' => 'Fake error message',
        ]);
        $response->assertStatus(302);
    }
}
