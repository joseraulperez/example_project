<?php

namespace Tests\Feature\Admin\Controllers\Posts;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Mockery;
use Tests\TestCase;

class DeleteControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Log in a user with a specific role.
     *
     * @param  $role
     */
    protected function logInUser($role)
    {
        $user = factory(User::class)->create([
            'role' => $role,
        ]);
        $this->be($user);
    }

    /** @test */
    public function if_post_exists_delete_from_database_and_redirects_to_posts_home()
    {
        $this->logInUser(User::ROLE_ADMIN);

        $post = factory(Post::class)->create();

        $this->assertDatabaseHas('posts', ['id' => $post->id]);

        $response = $this->get('/admin/posts/' . $post->id . '/delete');

        $this->assertDatabaseMissing('posts', ['id' => $post->id]);

        $response->assertRedirect('/admin/posts');
    }

    /** @test */
    public function only_delete_the_post_passed_by_argument()
    {
        $this->logInUser(User::ROLE_ADMIN);

        $posts = factory(Post::class, 2)->create();

        $this->assertDatabaseHas('posts', ['id' => $posts[0]->id]);
        $this->assertDatabaseHas('posts', ['id' => $posts[1]->id]);

        $response = $this->get('/admin/posts/' . $posts[0]->id . '/delete');

        $this->assertDatabaseMissing('posts', ['id' => $posts[0]->id]);
        $this->assertDatabaseHas('posts', ['id' => $posts[1]->id]);

        $response->assertRedirect('/admin/posts');
    }

    /** @test */
    public function if_the_database_fails_redirects_with_the_error_message()
    {
        $this->logInUser(User::ROLE_ADMIN);

        $post_mock = Mockery::mock(Post::class);
        $post_mock
            ->shouldReceive('where')
            ->once()
            ->andReturnSelf()
            ->shouldReceive('delete')
            ->once()
            ->andThrow(new \Exception('Fake error message'));

        $this->app->instance(Post::class, $post_mock);
        $response = $this->get('/admin/posts/1/delete');

        $response->assertSessionHas('message', [
            'level' => 'danger',
            'text' => 'Fake error message',
        ]);
        $response->assertStatus(302);
    }
}
