<?php

namespace Tests\Feature\Admin\Controllers\Posts;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class IndexControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Log in a user with a specific role.
     *
     * @param  $role
     */
    protected function logInUser($role)
    {
        $user = factory(User::class)->create([
            'role' => $role,
        ]);
        $this->be($user);
    }

    /** @test */
    public function posts_are_assigned_to_view()
    {
        $this->logInUser(User::ROLE_ADMIN);
        $posts = factory(Post::class, 2)->create();

        $response = $this->get('/admin/posts');

        $response->assertSee($posts[0]->title);
        $response->assertSee($posts[1]->title);
    }
}
