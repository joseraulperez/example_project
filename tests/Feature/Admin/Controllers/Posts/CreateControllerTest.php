<?php

namespace Tests\Feature\Admin\Controllers\Posts;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Config;
use Mockery;
use Tests\TestCase;

class CreateControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Log in a user with a specific role.
     *
     * @param  $role
     */
    protected function logInUser($role)
    {
        $user = factory(User::class)->create([
            'role' => $role,
        ]);
        $this->be($user);
    }

    /** @test */
    public function categories_and_locales_are_assigned_to_view()
    {
        $this->logInUser(User::ROLE_ADMIN);
        $categories = factory(Category::class, 2)->create();
        Config::set('locales', ['locale' => 'fake-locale']);

        $response = $this->get('/admin/posts/create');

        $response->assertSee($categories[0]->name);
        $response->assertSee($categories[1]->name);
        $response->assertSee('fake-locale');
    }

    /** @test */
    public function validation_fails_when_creating_a_post_if_rules_are_not_satisfied()
    {
        $this->logInUser(User::ROLE_ADMIN);
        Config::set('forms.rules.post', [
            'item' => 'required',
        ]);

        $response = $this->post('/admin/posts/create', [
            'wrong_item' => 'fake-data',
        ]);

        $response->assertSessionHasErrors(['item']);
    }

    /** @test */
    public function when_all_data_is_passed_a_post_is_stored_in_the_database()
    {
        $this->logInUser(User::ROLE_ADMIN);

        $fake_post = [
            'title' => 'Fake Title',
            'title_slug' => 'fake-title',
            'content' => 'fake content',
            'featured_image' => 'image.jpg',
            'locale' => 'es',
        ];

        $response = $this->post('/admin/posts/create', $fake_post);

        $this->assertDatabaseHas('posts', $fake_post);
        $response->assertRedirect('/admin/posts');
    }

    /** @test */
    public function if_the_database_fails_redirects_with_the_error_message()
    {
        $this->logInUser(User::ROLE_ADMIN);

        Config::set('forms.rules.post', [
            'item' => 'required',
        ]);

        $post_mock = Mockery::mock(Post::class);
        $post_mock
            ->shouldReceive('create')
            ->once()
            ->andThrow(new \Exception('Fake error message'));

        $this->app->instance(Post::class, $post_mock);
        $response = $this->post('/admin/posts/create', ['item' => 'fake-data']);

        $response->assertSessionHas('message', [
            'level' => 'danger',
            'text' => 'Fake error message',
        ]);
        $response->assertStatus(302);
    }
}
