<?php

namespace Tests\Feature\Admin\Controllers\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Mockery;
use Tests\TestCase;

class DeleteControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Log in a user with a specific role.
     *
     * @param  $role
     */
    protected function logInUser($role)
    {
        $user = factory(User::class)->create([
            'role' => $role,
        ]);
        $this->be($user);
    }

    /** @test */
    public function if_category_exists_delete_from_database_and_redirects_to_categories_home()
    {
        $this->logInUser(User::ROLE_ADMIN);

        $category = factory(Category::class)->create();

        $this->assertDatabaseHas('categories', ['id' => $category->id]);

        $response = $this->get('/admin/categories/' . $category->id . '/delete');

        $this->assertDatabaseMissing('categories', ['id' => $category->id]);

        $response->assertRedirect('/admin/categories');
    }

    /** @test */
    public function only_delete_the_category_passed_by_argument()
    {
        $this->logInUser(User::ROLE_ADMIN);

        $categories = factory(Category::class, 2)->create();

        $this->assertDatabaseHas('categories', ['id' => $categories[0]->id]);
        $this->assertDatabaseHas('categories', ['id' => $categories[1]->id]);

        $response = $this->get('/admin/categories/' . $categories[0]->id . '/delete');

        $this->assertDatabaseMissing('categories', ['id' => $categories[0]->id]);
        $this->assertDatabaseHas('categories', ['id' => $categories[1]->id]);

        $response->assertRedirect('/admin/categories');
    }

    /** @test */
    public function if_the_database_fails_redirects_with_the_error_message()
    {
        // TODO
/*        $this->logInUser(User::ROLE_ADMIN);

        $category_mock = Mockery::mock(Category::class);
        $category_mock
            ->shouldReceive('where')
            ->once()
            ->andReturnSelf()
            ->shouldReceive('delete')
            ->once()
            ->andThrow(new \Exception('Fake error message'));

        $this->app->instance(Category::class, $category_mock);
        $response = $this->get('/admin/categories/1/delete');

        $response->assertSessionHas('message', [
            'level' => 'danger',
            'text' => 'Fake error message',
        ]);
        $response->assertStatus(302);*/
    }
}
