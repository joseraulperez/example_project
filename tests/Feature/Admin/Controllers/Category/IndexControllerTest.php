<?php

namespace Tests\Feature\Admin\Controllers\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class IndexControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Log in a user with a specific role.
     *
     * @param  $role
     */
    protected function logInUser($role)
    {
        $user = factory(User::class)->create([
            'role' => $role,
        ]);
        $this->be($user);
    }

    /** @test */
    public function categories_are_assigned_to_view()
    {
        $this->logInUser(User::ROLE_ADMIN);
        $categories = factory(Category::class, 2)->create();

        $response = $this->get('/admin/categories');

        $response->assertSee($categories[0]->name);
        $response->assertSee($categories[1]->name);
    }
}
