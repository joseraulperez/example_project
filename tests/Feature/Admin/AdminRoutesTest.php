<?php

namespace Tests\Feature\Admin\Controllers\Posts;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AdminRoutesTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Log in a user with a specific role.
     *
     * @param  $role
     */
    protected function logInUser($role)
    {
        $user = factory(User::class)->create([
            'role' => $role,
        ]);
        $this->be($user);
    }

    /** @test */
    public function anonymous_users_get_404_error()
    {
        // Home
        $this->get('/admin')->assertStatus(404);

        // Posts
        $this->get('/admin/posts')->assertStatus(404);
        $this->get('/admin/posts/create')->assertStatus(404);
        $this->get('/admin/posts/1')->assertStatus(404);
        $this->get('/admin/posts/1/delete')->assertStatus(404);

        // Categories
        $this->get('/admin/categories')->assertStatus(404);
        $this->get('/admin/categories/create')->assertStatus(404);
        $this->get('/admin/categories/1')->assertStatus(404);
        $this->get('/admin/categories/1/delete')->assertStatus(404);
    }

    /** @test */
    public function not_admin_users_get_404_error()
    {
        $this->logInUser(User::ROLE_USER);

        // Home
        $this->get('/admin')->assertStatus(404);

        // Posts
        $this->get('/admin/posts')->assertStatus(404);
        $this->get('/admin/posts/create')->assertStatus(404);

        // Categories
        $this->get('/admin/categories')->assertStatus(404);
        $this->get('/admin/categories/create')->assertStatus(404);
    }

    /** @test */
    public function admin_users_get_200_response()
    {
        $this->logInUser(User::ROLE_ADMIN);

        // Home
        $this->get('/admin')->assertStatus(200);

        // Posts
        $this->get('/admin/posts')->assertStatus(200);
        $this->get('/admin/posts/create')->assertStatus(200);

        // Categories
        $this->get('/admin/categories')->assertStatus(200);
        $this->get('/admin/categories/create')->assertStatus(200);
    }
}
