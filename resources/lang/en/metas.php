<?php

return [
    // Home
    'home_title' => 'Zoolity',
    'home_description' => 'Zoolity is the magazine about pets & wild animals. Photos, videos and stories that bring you closer to the animal world.',
];