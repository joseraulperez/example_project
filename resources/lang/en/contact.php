<?php

return [
    'title' => 'Contact Us',
    'you_may_contact_us' => 'You may contact us at any time here: support@zoolity.com.',
    'we_will_do_our_best' => 'We will do our best to answer you as soon as possible :)',
];