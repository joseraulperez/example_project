<?php

return [
    'home' => 'Home',
    'contact_us' => 'Contact Us',
    'privacy_policy' => 'Privacy Policy',
    'terms_of_service' => 'Terms of Service',
    'other_languages' => 'Zoolity in other languages',
];