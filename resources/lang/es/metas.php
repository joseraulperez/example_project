<?php

return [
    // Home
    'home_title' => 'Zoolity',
    'home_description' => 'Zoolity es tu magazine de mascotas y animales salvajes. Fotos, videos e historias que te acercarán al mundo animal.',
];