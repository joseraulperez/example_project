@extends('layouts.clean')

@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-4">
                <div class="card mt-5 mb-4">
                    <div class="card-body">

                        <h4 class="mb-4 text-center">Welcome</h4>

                        {!! Form::open(['method' => 'post']) !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            {!! Form::label('email', 'Correo Electrónico', ['class' => 'sr-only']) !!}
                            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Correo Electrónico', 'required', 'autofocus']) !!}
                            @if ($errors->has('email'))
                                <span class="form-control-feedback">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            {!! Form::label('password', 'Contraseña', ['class' => 'sr-only']) !!}
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Contraseña', 'required']) !!}
                            @if ($errors->has('password'))
                                <span class="form-control-feedback">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">
                                Login
                            </button>
                        </div>

                        <p class="text-center mb-0"><a class="btn btn-link" href="{{ route('password.request') }}">¿Olvidaste la contraseña?</a></p>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
