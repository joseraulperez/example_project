@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Edit Post</h2>
        {!! Form::model($post, ['method' => 'post']) !!}
            @include('admin.posts.form')
        {!! Form::close() !!}
    </div>
@endsection