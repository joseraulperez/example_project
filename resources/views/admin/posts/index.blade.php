@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="d-flex  justify-content-between mb-3">
            {!! Form::open(['class' => 'form-inline', 'method' => 'get']) !!}
                {!! Form::submit('Filter', ['class'=>'btn btn-light']) !!}
            {!! Form::close() !!}

            <a class="btn btn-primary" href="{{ route('admin_posts_create') }}">New Post</a>
        </div>

        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Visible</th>
                <th>Locale</th>
                <th>Title</th>
                <th>Action</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
            <tr>
                <td>{{ $post->id }}</td>
                <td>{{ $post->visible }}</td>
                <td>{{ $post->locale }}</td>
                <td>{{ $post->title }}</td>
                <td><a class="btn btn-light" href="{{ route('admin_posts_edit', [$post->id]) }}">Edit</a></td>
                <td><a class="btn btn-danger delete_button" href="{{ route('admin_posts_delete', [$post->id]) }}">Delete</a></td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        $(".delete_button").click(function(){
            if(!confirm("Are you sure you want to delete this?")){
                return false;
            }
        });
    </script>
@endsection