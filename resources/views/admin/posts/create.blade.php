@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Create Post</h2>
        {!! Form::open(['method' => 'post']) !!}
        @include('admin.posts.form')
        {!! Form::close() !!}
    </div>
@endsection