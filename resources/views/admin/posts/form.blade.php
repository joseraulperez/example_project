<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    {!! Form::label('title', 'Title') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('title_slug', 'Title Slug') !!}
                    {!! Form::text('title_slug', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('excerpt', 'Excerpt') !!}
                    {!! Form::textarea('excerpt', null, ['class' => 'form-control', 'rows' =>'2']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('content', 'Content') !!}
                    {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => '40']) !!}
                </div>


            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('visible', 'Visible') !!}
                    {!! Form::checkbox('visible') !!}
                </div>

                <hr>

                <div class="form-group">
                    {!! Form::label('featured_image', 'Featured Image') !!}
                    <div class="alert alert-info">
                        <small>
                            https://d3vzweelfkjzoo.cloudfront.net
                        </small>
                    </div>
                    @if(!empty($post['featured_image']))
                        <img class="center-block img-thumbnail" src="{{$post['featured_image']}}">
                    @endif
                    {!! Form::text('featured_image', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('featured_image_source_name', 'Source Name') !!}
                    {!! Form::text('featured_image_source_name', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('featured_image_source_link', 'Source Link') !!}
                    {!! Form::text('featured_image_source_link', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('featured_image_description', 'Description') !!}
                    {!! Form::text('featured_image_description', null, ['class' => 'form-control']) !!}
                </div>

                <hr>

                <div class="form-group">
                    {!! Form::label('locale', 'Locale') !!}
                    {!! Form::select('locale', $locales, null, ['class' => 'form-control']) !!}
                </div>

                <div class='form-group'>
                    {!! Form::label('category_id', 'Category') !!}
                    {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'placeholder' => '']) !!}
                </div>

                <hr>

                <fieldset>
                    <legend>Meta Data</legend>

                    <div class="form-group">
                        {!! Form::label('meta_title', 'Title') !!}
                        {!! Form::text('meta_title', null, ['class' => 'form-control']) !!}
                        <p class="help-block">Keep this title under 60 characters. Current: <span id="meta-title-length">0</span></p>
                    </div>

                    <div class="form-group">
                        {!! Form::label('meta_description', 'Description') !!}
                        {!! Form::textarea('meta_description', null, ['class' => 'form-control', 'rows' =>'4']) !!}
                        <p class="help-block">Keep this description under 160 characters. Current: <span id="meta-description-length">0</span></p>
                    </div>

                    <div class="form-group">
                        {!! Form::label('meta_keywords', 'Keywords') !!}
                        {!! Form::text('meta_keywords', null, ['class' => 'form-control']) !!}
                    </div>

                </fieldset>
            </div>
        </div>

        <div class='form-group'>
            {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        </div>
    </div>
</div>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea#content',
        menubar: true,
        plugins: "lists, media, image, link, code, wordcount",
        image_caption: true,
        rel_list: [
            {title: 'follow', value: 'follow'},
            {title: 'nofollow', value: 'nofollow'}
        ]
    });

    printMetaTitleLength();
    $("input[name='meta_title']").keyup(function(){
        printMetaTitleLength();
    });

    printMetaDescriptionLength();
    $("textarea[name='meta_description']").keyup(function(){
        printMetaDescriptionLength();
    });

    function printMetaTitleLength() {
        $("#meta-title-length").text($("input[name='meta_title']").val().length);
    }

    function printMetaDescriptionLength() {
        $("#meta-description-length").text($("textarea[name='meta_description']").val().length);
    }
</script>