@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Edit Photo</h2>

        @include('admin.photos.gallery')

        {!! Form::model($photo, ['method' => 'post']) !!}
            @include('admin.photos.form')
        {!! Form::close() !!}
    </div>
@endsection