<div class="card mb-4">
    <div class="card-body">
        <ul class="mb-0">
            <li><strong>id</strong> {{ $gallery->id }}</li>
            <li><strong>title</strong> {{ $gallery->title }}</li>
            <li><strong>locale</strong> {{ $gallery->locale }}</li>
        </ul>
    </div>
</div>