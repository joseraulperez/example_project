@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Create Photo</h2>

        @include('admin.photos.gallery')

        {!! Form::open(['method' => 'post']) !!}
            @include('admin.photos.form')
        {!! Form::close() !!}
    </div>
@endsection