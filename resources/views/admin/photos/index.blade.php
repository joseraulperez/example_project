@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Photos</h2>

        @include('admin.photos.gallery')

        <a class="btn btn-primary mb-2" href="{{ route('admin_photos_create', [$gallery->id]) }}">Create</a>

        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Position</th>
                <th>Title</th>
                <th>Slug</th>
                <th>Image</th>
                <th>Actions</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($photos as $photo)
            <tr>
                <td>{{ $photo->id }}</td>
                <td>{{ $photo->position }}</td>
                <td>{{ $photo->title }}</td>
                <td>{{ $photo->title_slug }}</td>
                <td>{{ $photo->image }}</td>
                <td>
                    <a class="btn btn-light" href="{{ route('admin_photos_edit', [$gallery->id, $photo->id]) }}">Edit</a>
                </td>
                <td>
                    <a class="btn btn-danger delete_button" href="{{ route('admin_photos_delete', [$gallery->id, $photo->id]) }}">Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        $(".delete_button").click(function(){
            if(!confirm("Are you sure you want to delete this?")){
                return false;
            }
        });
    </script>
@endsection