@extends('layouts.admin')

@section('content')
    <div class="container">

        <a class="btn btn-primary mb-2" href="{{ route('admin_galleries_create') }}">Create</a>

        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Locale</th>
                <th>Title</th>
                <th>Slug</th>
                <th>Relations</th>
                <th>Actions</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($galleries as $gallery)
            <tr>
                <td>{{ $gallery->id }}</td>
                <td>{{ $gallery->locale }}</td>
                <td>{{ $gallery->title }}</td>
                <td>{{ $gallery->title_slug }}</td>
                <td>
                    <a class="btn btn-light" href="{{ route('admin_photos', [$gallery->id]) }}">
                        <i class="fa fa-picture-o"></i>
                        Photos
                    </a>
                </td>
                <td>
                    <a class="btn btn-light" href="{{ route('admin_galleries_edit', [$gallery->id]) }}">Edit</a>
                </td>
                <td>
                    <a class="btn btn-danger delete_button" href="{{ route('admin_galleries_delete', [$gallery->id]) }}">Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        $(".delete_button").click(function(){
            if(!confirm("Are you sure you want to delete this?")){
                return false;
            }
        });
    </script>
@endsection