@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Create Gallery</h2>
        {!! Form::open(['method' => 'post']) !!}
            @include('admin.galleries.form')
        {!! Form::close() !!}
    </div>
@endsection