@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Edit Gallery</h2>
        {!! Form::model($gallery, ['method' => 'post']) !!}
            @include('admin.galleries.form')
        {!! Form::close() !!}
    </div>
@endsection