<div class="card mb-4">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('visible', 'Visible') !!}
                    {!! Form::checkbox('visible') !!}
                </div>
                <div class='form-group'>
                    {!! Form::label('locale', 'Locale') !!}
                    {!! Form::select('locale', $locales, null, ['class' => 'form-control', 'placeholder' => '']) !!}
                </div>
                <div class='form-group'>
                    {!! Form::label('title', 'Title') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>
                <div class='form-group'>
                    {!! Form::label('title_slug', 'Title Slug') !!}
                    {!! Form::text('title_slug', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('excerpt', 'Excerpt') !!}
                    {!! Form::textarea('excerpt', null, ['class' => 'form-control', 'rows' =>'2']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <fieldset>
                    <legend>Meta Data</legend>

                    <div class="form-group">
                        {!! Form::label('meta_title', 'Title') !!}
                        {!! Form::text('meta_title', null, ['class' => 'form-control']) !!}
                        <p class="help-block">Keep this title under 60 characters. Current: <span id="meta-title-length">0</span></p>
                    </div>

                    <div class="form-group">
                        {!! Form::label('meta_description', 'Description') !!}
                        {!! Form::textarea('meta_description', null, ['class' => 'form-control', 'rows' =>'4']) !!}
                        <p class="help-block">Keep this description under 160 characters. Current: <span id="meta-description-length">0</span></p>
                    </div>

                    <div class="form-group">
                        {!! Form::label('meta_keywords', 'Keywords') !!}
                        {!! Form::text('meta_keywords', null, ['class' => 'form-control']) !!}
                    </div>

                </fieldset>
            </div>
        </div>
        <div class='form-group'>
            {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        </div>
    </div>
</div>