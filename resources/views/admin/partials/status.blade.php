@if($status)
    <span class="text-success">✔</span>
@else
    <span class="text-danger">✖</span>
@endif