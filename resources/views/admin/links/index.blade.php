@extends('layouts.admin')

@section('content')
    <div class="container">

        <a class="btn btn-primary mb-2" href="{{ route('admin_links_create') }}">Create</a>

        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Internal Link</th>
                <th>Original Link</th>
                <th>Affiliation Link</th>
                <th>Active</th>
                <th>Clicks</th>
                <th>Actions</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($links as $link)
            <tr>
                <td>{{ $link->id }}</td>
                <td>{{ route('main_go_to', [$link->id]) }}</td>
                <td>{{ $link->original_link }}</td>
                <td>{{ $link->affiliation_link }}</td>
                <td>{{ $link->is_active }}</td>
                <td>{{ $link->clicks }}</td>
                <td><i class="fa {{ $link->icon }}"></i></td>
                <td>
                    <a class="btn btn-light" href="{{ route('admin_links_edit', [$link->id]) }}">Edit</a>
                </td>
                <td>
                    <a class="btn btn-danger delete_button" href="{{ route('admin_links_delete', [$link->id]) }}">Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        $(".delete_button").click(function(){
            if(!confirm("Are you sure you want to delete this?")){
                return false;
            }
        });
    </script>
@endsection