@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Create Link</h2>
        {!! Form::open(['method' => 'post']) !!}
            @include('admin.links.form')
        {!! Form::close() !!}
    </div>
@endsection