<div class="card mb-4 p-4">
    <div class="card-block">
        <div class='form-group'>
            {!! Form::label('original_link', 'Original Link') !!}
            {!! Form::text('original_link', null, ['class' => 'form-control']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('affiliation_link', 'Affiliation Link') !!}
            {!! Form::text('affiliation_link', null, ['class' => 'form-control']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('is_active', 'Active') !!}
            {!! Form::checkbox('is_active', 1) !!}
        </div>
        <div class='form-group'>
            {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        </div>
    </div>
</div>