@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Edit Link</h2>
        {!! Form::model($link, ['method' => 'post']) !!}
            @include('admin.links.form')
        {!! Form::close() !!}
    </div>
@endsection