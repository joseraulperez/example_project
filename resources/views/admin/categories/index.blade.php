@extends('layouts.admin')

@section('content')
    <div class="container">

        <a class="btn btn-primary mb-2" href="{{ route('admin_categories_create') }}">Create</a>

        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Parent Id</th>
                <th>Locale</th>
                <th>Name</th>
                <th>Slug</th>
                <th>Icon</th>
                <th>Actions</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
            <tr>
                <td>{{ $category->id }}</td>
                <td>{{ $category->parent_id }}</td>
                <td>{{ $category->locale }}</td>
                <td>{{ $category->name }}</td>
                <td>{{ $category->name_slug }}</td>
                <td><i class="fa {{ $category->icon }}"></i></td>
                <td>
                    <a class="btn btn-light" href="{{ route('admin_categories_edit', [$category->id]) }}">Edit</a>
                </td>
                <td>
                    <a class="btn btn-danger delete_button" href="{{ route('admin_categories_delete', [$category->id]) }}">Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        $(".delete_button").click(function(){
            if(!confirm("Are you sure you want to delete this?")){
                return false;
            }
        });
    </script>
@endsection