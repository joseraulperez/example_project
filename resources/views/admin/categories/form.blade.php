<div class="card mb-4 p-4">
    <div class="card-block">
        <div class='form-group'>
            {!! Form::label('locale', 'Locale') !!}
            {!! Form::select('locale', $locales, null, ['class' => 'form-control', 'placeholder' => '']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('parent_id', 'Parent') !!}
            {!! Form::select('parent_id', $categories, null, ['class' => 'form-control', 'placeholder' => '']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('name_slug', 'Name Slug') !!}
            {!! Form::text('name_slug', null, ['class' => 'form-control']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('icon', 'Icon') !!}
            {!! Form::text('icon', null, ['class' => 'form-control']) !!}
        </div>
        <div class='form-group'>
            {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        </div>
    </div>
</div>