@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Edit Category</h2>
        {!! Form::model($category, ['method' => 'post']) !!}
            @include('admin.categories.form')
        {!! Form::close() !!}
    </div>
@endsection