@extends('layouts.admin')

@section('content')
    <div class="container">
        <h2 class="mb-4">Create Category</h2>
        {!! Form::open(['method' => 'post']) !!}
            @include('admin.categories.form')
        {!! Form::close() !!}
    </div>
@endsection