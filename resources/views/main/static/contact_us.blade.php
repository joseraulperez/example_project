@extends('layouts.clean')

@section('body_class'){{'home'}}@endsection
@section('meta_title'){{'Zoolity'}}@endsection
@section('meta_description'){{''}}@endsection
@section('content')

    <div class="d-flex justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h2 class="mb-4">{{ trans('contact.title') }}</h2>
                    <p>{{ trans('contact.you_may_contact_us') }}</p>
                    <p>{{ trans('contact.we_will_do_our_best') }}</p>
                </div>
            </div>
        </div>
    </div>

@endsection