@extends('layouts.clean')

@section('body_class'){{'home'}}@endsection
@section('meta_title'){{'Zoolity'}}@endsection
@section('meta_description'){{''}}@endsection
@section('content')

    <div class="d-flex justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h2>Zoolity.com Privacy and Cookies Policy</h2>
                    <p>This privacy and cookies policy governs our processing of ‘Personally identifiable information’ (PII) provided to us or to which we access through the use of the website www.zoolity.com or related sites and applications. The PII provided to us as stated above shall be processed in a personal data file whose controller is "<b>Zoolity</b>" or "<b>we</b>", with registered office at Pablo Iglesias n8 Barcelona, Spain. Zoolity has adopted technical and organizational measures to guarantee the security and integrity of PII, and to avoid their loss, alteration, and/or access by unauthorized third parties, as required under applicable regulations. Please read our privacy and cookies policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your PII. By accessing our sites or applications, you are agreeing to the collection, use, disclosure and other handling of your PII as described below.</p>

                    <h4>When do we collect information?</h4>
                    <p>We collect PII from you when you register on our sites or apps, subscribe to a newsletter, fill out a form or enter information on or use our site or app. If, when requested, you omit PII requested as compulsory, this could make it impossible for us to comply with your request or to provide you the service ordered under the agreed conditions. You shall immediately notify us of any modification to your PII to ensure that the information contained in our files is at all times updated and free of errors.</p>

                    <h4>How do we use your information?</h4>
                    <p>We may use the PII we collect in the following ways:</p>
                    <p>To manage your requests and provide our services, personalize user’s experience and to allow us to deliver the type of content and offers in which you are most interested.</p>

                    <ul>
                        <li>To improve our sites and apps in order to better serve you.</li>
                        <li>To send commercial communications regarding your orders or other products and services.</li>
                    </ul>

                    <h4>Rights to access, rectify, cancel and oppose to any processed PII</h4>
                    <p>Zoolity guarantees the exercise of the rights to access, rectify, cancel and oppose to the processing of PII under the terms of the applicable data protection regulations.</p>

                    <h4>Who do we share your Personal Information with?</h4>
                    <p>We do not sell, trade, or otherwise transfer to outside parties your PII. We may share your PII with trusted partners and suppliers only if they need such information to perform their specific obligations (e.g. to provide you with our service, to collect, store and process your PII, to select and serve relevant adverts, and to improve and optimize our sites and apps), and we require them to do so based on our instructions and in compliance with this Privacy and Cookies Policy and any other appropriate confidentiality and security measures and applicable regulations.</p>

                    <h6>Any other information may only be shared with your prior consent. </h6>
                    <p>Occasionally, at our discretion, we may include or offer third-party products or services on our sites and apps. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>

                    <h4>Do we use ‘cookies’?</h4>
                    <p>Yes. Cookies are small files that a site transfers to your computer’s or device’s hard drive through your Web browser (if you allow) that enables the site’s systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember your preferred language. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide, support and improve our service. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>

                    <h6>We use cookies in a few different ways under following categories:</h6>
                    <p>Necessary cookies. These are cookies that are required for the operation of our sites or apps. They include, for example, cookies that enable you to log into our sites and apps.</p>
                    <p>Functionality cookies. These are required for our sites or apps to function correctly and to give you access to our service. For example, these cookies allow our sites or apps remember your settings and provide enhanced and more personal features.</p>
                    <p>Performance cookies. These allow us to recognise and count visitors and to see how visitors move around our sites and apps. This helps us to improve the functioning of our sites and apps.</p>
                    <p>Targeting or Advertising cookies. These cookies record visits to our sites and apps. This information enable us to make our sites and apps more interesting for you.</p>

                    <h4>Can you switch off cookies? </h4>
                    <p>To switch off cookies, you need to change your browser settings.</p>
                    <p>You may set up, freely and at any time, the installation of cookies through your browser. Information about it may be consulted at:</p>
                    <ul>
                        <li>Firefox: <a href="https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences">https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences</a></li>
                        <li>Chrome: <a href="https://support.google.com/chrome/answer/95647?hl=en">https://support.google.com/chrome/answer/95647?hl=en</a></li>
                        <li>Google Chrome for Android: <a href="https://support.google.com/chrome/answer/2392971?hl=en">https://support.google.com/chrome/answer/2392971?hl=en</a></li>
                        <li>Explorer: &nbsp;<a href="https://windows.microsoft.com/en-GB/internet-explorer/delete-manage-cookies#ie=ie-11">https://windows.microsoft.com/en-GB/internet-explorer/delete-manage-cookies#ie=ie-11</a></li>
                        <li>Safari: <a href="https://support.apple.com/en-gb/HT201265">https://support.apple.com/en-gb/HT201265</a></li>
                        <li>Opera: &nbsp;<a href="https://help.opera.com/Windows/11.60/en/cookies.html">https://help.opera.com/Windows/11.60/en/cookies.html</a></li>
                    </ul>
                    <p>For further information about how to do this please visit <a href="https://www.aboutcookies.org/">https://www.aboutcookies.org/</a>.</p>
                    <p>Please note that if you choose to switch off cookies on your device, that will mean certain functions of our sites or apps cannot operate properly and the usability of the our services may be affected.</p>
                    <p>For further information regarding cookies and behavioural &nbsp;advertising: <a href="https://www.allaboutcookies.org">https://www.allaboutcookies.org</a> and <a href="https://www.youronlinechoices.com">https://www.youronlinechoices.com</a></p>
                    <h2>Contacting Us</h2><b>
                    </b>
                    <p>If there are any questions regarding this privacy and cookies policy you may contact us here: support@Zoolity.com.</p>
                </div>
            </div>
        </div>
    </div>

@endsection