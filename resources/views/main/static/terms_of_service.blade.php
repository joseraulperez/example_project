@extends('layouts.clean')

@section('body_class'){{'home'}}@endsection
@section('meta_title'){{'Zoolity'}}@endsection
@section('meta_description'){{''}}@endsection
@section('content')

    <div class="d-flex justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h2>Terms of Use for Zoolity.com</h2>
                    <p>The use of any page on the website Zoolity.com, including any of its subdomains and/or sections
                        (hereafter ‘ZOOLITY’), as well as the use of any service offered on ZOOLITY or the download of
                        computer programs from ZOOLITY, implies the full acceptance of these terms of use.</p>
                    <p>Zoolity is a company domiciled at Pablo Iglesias n8, Barcelona, Spain.</p>
                    <p>The present terms of use are divided into two sections:</p>
                    <ul>
                        <li><strong>I General website terms of use applicable to all of Zoolity and to all products
                                and/or services offered or sold thereon.</strong></li>
                    </ul>
                    <ul>
                        <li><strong>II Intellectual and industrial property rights of Zoolity content and of the
                                contributions made by developers or users to ZOOLITY.</strong></li>
                    </ul>
                    <p>The user shall be entirely liable for any losses or damages suffered by Zoolity that does not
                        comply with the provisions established in any
                        part of these terms of use or in any other section of the ZOOLITY website. The user further
                        agrees to indemnify and hold harmless the company Zoolity, as well as its
                        administrators, personnel, agents, and representatives, from any claims in which they may be
                        implicated as a consequence of such user noncompliance.</p>
                    <h2>I GENERAL WEBSITE TERMS OF USE APPLICABLE TO ALL OF ZOOLITY AND TO ALL PRODUCTS AND/OR SERVICES
                        OFFERED OR SOLD THEREON.</h2>
                    <p>1.1 Objective of the General Terms of Use</p>
                    <p>ZOOLITY offers now or in the future, the services,
                        products, and complementary facilities as detailed below:</p>
                    <ul>
                        <li>a. Access to a respository of software content classified by category. This includes the
                            download of software from ZOOLITY’s servers with the highest assurance of availability
                            and/or speed that this may involve. In no case does this imply the sale of a license nor
                            possession of intellectual property rights for the program, the use of which will be subject
                            to the license, conditions, and restrictions imposed by the developer or intellectual
                            property rights holder.
                        </li>
                        <li>b. Search service for software content on the ZOOLITY library and directory.</li>
                        <li>c. Direct email update service for new programs and updates.</li>
                        <li>d. Periodic newsletter service for registered users covering new developments and
                            information related to the sector or to ZOOLITY.
                        </li>
                        <li>e. ZOOLITY may, at some point, provide additional services or facilities, either
                            free or paid.
                        </li>
                    </ul>
                    <p>1.2 Liability for the processing and use of content</p>
                    <p>ZOOLITY does not guarantee the quality, accuracy, reliability, correctness, or morality of the
                        data, programs, information, or opinions included on the site and in particular those expressed
                        or entered in user forums or opinions. However, users may report presumed abuses or
                        inappropriate uses of the site to allow ZOOLITY to review the corresponding content. In
                        relation to the content and programs available for free download, ZOOLITY limits itself to
                        offering an intermediary service, as established in Article 17 of Law 34/2002 of 11 July on
                        Information Society Services and E-Commerce (‘LSSI’), by providing links and search content,
                        while not assuming liability for said content beyond the cases expressly provided for in the
                        aforementioned law; that is, ZOOLITY shall only accept liability when it (i) has actual
                        knowledge that the activity or information being reported or referred is illegal or harms the
                        assets or rights of a third party covered by compensation, and (ii) has not acted with diligence
                        to remove or disable the corresponding link. The service provider shall be understood to have
                        actual knowledge when a competent authority has declared the data illegal or ordered its
                        withdrawal, or access to said data has become impossible, or the existence of the damage has
                        been declared, and the service provider is aware of the corresponding resolution.</p>
                    <p>An identical system to the one established in the previous paragraph applies to other links to
                        third-party sites that may be available on ZOOLITY. The inclusion of such links does not imply
                        any relationship, recommendation, or supervision by ZOOLITY of the destination page, and
                        ZOOLITY therefore accepts no liability for its content, except in such cases as expressly
                        established in the LSSI.</p>
                    <p>The apps published on ZOOLITY are products created by their respective developers. This website
                        is not directly affiliated with them. All of the brands, trademarks, product and company names
                        or logos mentioned here are property of their respective owners. Our download site and app
                        distribute original, unaltered software, which is obtained directly from the developers’
                        websites and is not modified in any way.</p>
                    <p>The user assumes as his or her exclusive liability any consequences, damages, or actions that may
                        arise from accessing, playing, or disseminating said content. It is the responsibility of the
                        user to verify, prior to downloading, the compatibility of the program with his or her computer
                        and other applications, as well as its features and the advantages of downloading and installing
                        the program on his or her system. Likewise, it is the responsibility of the user to ensure that
                        the download and use of ZOOLITY content is legal and appropriate in the user’s location and
                        personal circumstances.</p>
                    <p>ZOOLITY shall not be held liable for the infringements of any developer, programmer, or user
                        that affect the rights of another ZOOLITY user, or of a third party, including rights related
                        to copyright, trademarks, patents, confidential information, or any other intellectual or
                        industrial property rights.</p>
                    <p>This limited guarantee and limitation of liability mentioned in the preceding paragraphs shall
                        neither affect nor prejudice the obligatory rights extended by place of residence and applicable
                        by direct application of Spanish mandatory law or by the forwarding of Spanish law to the laws
                        of another jurisdiction for specific cases.</p>
                    <p>1.4 Unauthorized use of the ZOOLITY website or its content</p>
                    <p>The user may not employ the facilities and capacities of the ZOOLITY site to carry out or
                        propose activities prohibited by law, or to attempt to attract site users to competitor
                        services, or to advertise the sale of products or services for commercial purposes. The user
                        shall refrain from interfering with ZOOLITY or other users’ use of the ZOOLITY site, and in
                        particular from impersonating another user or person while using the portal. Likewise, the user
                        agrees not to add information to forums that is not relevant to that particular forum.</p>
                    <p>Access via robots and/or scripts to access, copy, or control any part of ZOOLITY are prohibited
                        without the express prior authorization of ZOOLITY.</p>
                    <p>It is strictly prohibited to add serial numbers, cracked software, or similar information, as
                        well as links to pages that include them, in ZOOLITY’s forums or comments. The IP addresses of
                        offenders shall be saved and appropriate legal measures taken against them. Likewise, the user
                        shall be responsible for ensuring compliance with these clauses by any person he or she
                        authorizes to use the service.</p>
                    <p>1.5 Liability for the operation of ZOOLITY</p>
                    <p>EXCEPT IN CASES OF INTENTIONAL ACTION, AND THOSE IN WHICH DUE TO THE PARTICULAR CIRCUMSTANCES OF
                        THE USER OR OBJECT AN IMPERATIVE REGIME OF LIABILITY IS DEEMED APPLICABLE, ZOOLITY
                        SHALL NOT BE LIABLE FOR ANY LOSSES OR DAMAGES OF ANY NATURE AFFECTING THE USER DUE TO THE USE OF
                        ZOOLITY OR OF ANY OF THE SERVICES OR PRODUCTS OFFERED ON THE WEBSITE.</p>
                    <p>IN PARTICULAR, AND WITHOUT LIMITING THE PROVISIONS OF THE PREVIOUS PARAGRAPH, ZOOLITY ASSUMES
                        NO RESPONSIBILITY FOR THE CONTENT OF THE PROGRAMS AVAILABLE FOR DOWNLOAD THROUGH ZOOLITY, WHICH
                        ARE THE EXCLUSIVE RESPONSIBILITY OF THEIR DEVELOPERS, AND SUBJECT TO THE CORRESPONDING LICENSE
                        CONDITIONS. THE USER ASSUMES ALL RISKS ASSOCIATED WITH THE DOWNLOAD AND USE OF THE PROGRAMS,
                        EXPRESSLY EXONERATING ZOOLITY OF ALL LIABILITY, AND ACKNOWLEDGING THAT ZOOLITY,
                        EXCEPT IN CASES IN WHICH IT IS EXPRESSLY ESTABLISHED OTHERWISE, IS LIMITED TO SOLELY PROVIDING
                        ACCESS TO THE PROGRAMS OF THE CORRESPONDING CREATOR OR DEVELOPER.</p>
                    <p>1.6 Service suspensions</p>
                    <p>Zoolity may unilaterally cease to provide to any or all users the services and/or products
                        offered via the ZOOLITY website.</p>
                    <p>In cases of service interruption to a particular user, a simple notification to the user shall
                        suffice for said interruption, for any situation involving noncompliance with the terms of use
                        or with any applicable regulation. The user accepts that Zoolity shall incur no liability on
                        the occasion of a suspension on these terms.</p>
                    <p>Additionally, Zoolity may at any time, and incurring no liability, vary the content of
                        ZOOLITY, interrupt the provision of all or some services, and/or deactivate or delete all or
                        some user accounts without further limitations beyond those established by applicable laws on
                        the requirement to maintain logs of certain operations during the corresponding legal time
                        periods.</p>
                    <p>1.7 Applicable law and jurisdiction</p>
                    <p>The present terms of use are subject to Spanish legislation and to the extent to which the law
                        does not establish an obligatory jurisdiction for a particular case the parties are subject to
                        the exclusive jurisdiction of the courts of Málaga in everything related to them.</p>
                    <h2>II INTELLECTUAL AND INDUSTRIAL PROPERTY RIGHTS TO ZOOLITY CONTENT AND CONTRIBUTIONS MADE BY
                        DEVELOPERS AND ZOOLITY USERS</h2>
                    <p>2.1 Rights to ZOOLITY content</p>
                    <p>The user acknowledges that the elements and utilities integrated into the ZOOLITY website
                        include content belonging to ZOOLITY, content from third-party developers, and content created
                        by users themselves. Content belonging to ZOOLITY (including, with no limitation, the
                        trademarks, logos, ZOOLITY site HTML code, design, and program descriptions and remaining
                        content) and content from third-party developers are protected by intellectual and/or industrial
                        property legislation.</p>
                    <p>The user therefore commits to respect the terms and conditions established in the present general
                        conditions of use and the corresponding licenses for the use of the content from third- party
                        developers available on ZOOLITY. The user acknowledges that the reproduction, modification,
                        transformation, public communication or distribution, including without limitation the
                        commercialization, decompilation, disassembling, utilization of reverse engineering techniques
                        or any other method to obtain the source code of the website and/or applications available on
                        it, and transformation or publication of any unauthorized test result for any of the elements or
                        utilities integrated into the ZOOLITY website constitutes an infraction of intellectual and/or
                        industrial property rights, and the user is therefore obligated to refrain from engaging in any
                        of the aforementioned actions.</p>
                    <p>The user agrees to neither remove nor alter any distinctive sign used as a trademark or
                        commercial name (graphics, logos, etc.), identifying feature of an element protected by
                        copyright, or other notices, captions, symbols, or labels of Zoolity or of third
                        parties that appear on the ZOOLITY website.</p>
                    <p>2.2 Use of the content and applications available on ZOOLITY</p>
                    <p>Unless prior agreement between the rights holder and the user stipulates otherwise, the user
                        agrees to utilize the information and applications available on ZOOLITY exclusively for his or
                        her own needs and to neither directly nor indirectly engage in commercial exploitation of the
                        products and/or services to which he or she has access, or of the results obtained due to
                        utilization of the ZOOLITY website.</p>
                    <p>2.3 Prohibited conduct</p>
                    <p>The user shall abstain from any conduct in the use of the ZOOLITY website that jeopardizes the
                        intellectual and industrial property rights of Zoolity or of third parties; or that
                        infringes or transgresses the honor, personal or family intimacy, or the image of third parties;
                        or that is illicit or damaging to morality.</p>
                    <p>In particular, and without implying any limitation, the user shall abstain from engaging, via the
                        use of the ZOOLITY website, in any destruction, alteration, disablement, or damage to the
                        electronic data, programs, or documents belonging to Zoolity, to its providers, or to
                        third parties, nor shall the user introduce or spread on the network any programs, viruses,
                        applets, Active X controls, or any physical or electronic instrument or device that causes or is
                        susceptible to causing any kind of alteration to the network, the system, or third-party
                        computers. Likewise, it is expressly prohibited to engage in any type of activity or practice
                        that violates the principles of good conduct generally accepted amongst Internet users.</p>
                    <p>2.4 Content uploaded on ZOOLITY by the user</p>
                    <p>The user is solely responsible for all materials uploaded, posted, e-mailed, or added in any
                        other way by him or her to ZOOLITY (‘User Content’). The user certifies that he or she holds
                        all intellectual and/or industrial property rights to User Content. By using the website and in
                        exchange for the broadcasting of the corresponding content, the user cedes to Zoolity
                        and its associated and linked companies, as well as its collaborators, an irrevocable, free, and
                        non-exclusive global license, liable for sublicense, for a duration equal to the duration of the
                        corresponding rights, to reproduce, transform, distribute, or publicly transmit User Content on
                        ZOOLITY and the pages of its affiliates, related firms, and/or collaborators. ZOOLITY may
                        distribute User Content to other users or make any other future use of it via any medium and/or
                        procedure and in any format.</p>
                    <p>2.5 Requirement to report noncompliance</p>
                    <p>Any presumed infraction of intellectual or industrial property rights carried out on ZOOLITY and
                        identified by a user should be reported to ZOOLITY by sending an email that includes a
                        description of the presumed infraction to the following address: support@Zoolity.com.</p>
                    <h2>External Graphic Resources</h2>
                    <p>Thanks to our friends from <a rel="nofollow" href="https://icons8.com/">icons8</a> for so great icons and
                        service :).</p>
                </div>
            </div>
        </div>
    </div>

@endsection