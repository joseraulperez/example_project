@extends('layouts.main')

@section('body_class'){{'home'}}@endsection
@section('meta_title'){{ metas()->title()->app($app) }}@endsection
@section('meta_description'){{ metas()->description()->app($app) }}@endsection
@section('content')

    <div class="d-flex justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card card-big-app">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex justify-content-row">
                            <div class="card-big-app-icon-wrapper">
                                <img class="card-big-app-icon card-big-app-icon-{{ $app->platform->name_slug }}" src="{{ image()->get($app->icon_200x200) }}">
                            </div>
                            <div>
                                @if($app->is_free)
                                    <span class="badge card-big-app-free">{{ trans('app.free_badge') }}</span>
                                @else
                                    <span class="badge card-big-app-paid">{{ config('currencies.'.$app->currency) }}{{ $app->price }}</span>
                                @endif
                                <h1 class="card-big-app-title">{{ utf8_decode(html_entity_decode($app->title)) }}</h1>
                                    @if(isset($app->developer) && !empty($app->developer->name))
                                        <span class="card-big-app-developer">{{ trans('app.by_developer', ['developer_name' => $app->developer->name]) }}</span>
                                    @endif
                                <p class="lead mt-2 mb-0">{{ $app->original_short_description }}</p>
                            </div>
                        </div>
                        <div>
                            <div class="mb-4">
                                <a class="btn card-big-app-btn-download btn-block" rel="nofollow" target="_blank" href="{{ $app->link }}">
                                    <div class="clearfix text-left">
                                        <span>
                                            {{ trans('app.button_download') }} @if($app->is_free){{ trans('app.free_badge') }}@endif
                                            <br>
                                            <small>{{ trans('app.button_safe') }}</small>
                                        </span>
                                        <i class="fa fa-external-link"></i>
                                    </div>
                                </a>
                            </div>
                            <div class=" text-center mb-0">
                                <div class="d-flex justify-content-center clearfix mb-1">
                                    <div class="c100 p{{ number_format($app->original_rating_value*100/5, 0) }} small">
                                        <span>{{ number_format($app->original_rating_value*10/5, 1) }}</span>
                                        <div class="slice">
                                            <div class="bar"></div>
                                            <div class="fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    @php
                                        $votes = array_fill(0, strlen($app->original_rating_votes), 0);
                                        $votes[0] = substr($app->original_rating_votes, 0, 1);
                                        $votes = number_format(implode($votes));
                                    @endphp
                                    <small class="text-muted">{{ trans('app.rating_votes', ['votes' => $votes]) }}</small>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <h4 class="title-comments mb-3">{{ trans('app.last_comments', ['quantity' => $app->last_comments->count()]) }}</h4>

                    <ul class="list-unstyled card-big-app-list-comments">
                        @foreach($app->last_comments as $comment)
                            <li>
                                <div class="d-flex justify-content-left align-items-center">
                                    @if($comment->store_rating >= 3)
                                        <div class="svg-container">
                                            <svg width="35" height="35" fill="#00b0ff" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1">
                                                <g id="surface1">
                                                    <path style=" " d="M 16 4 C 9.382813 4 4 9.382813 4 16 C 4 22.617188 9.382813 28 16 28 C 22.617188 28 28 22.617188 28 16 C 28 9.382813 22.617188 4 16 4 Z M 16 6 C 21.535156 6 26 10.464844 26 16 C 26 21.535156 21.535156 26 16 26 C 10.464844 26 6 21.535156 6 16 C 6 10.464844 10.464844 6 16 6 Z M 14.4375 8 L 14.15625 8.4375 C 14.15625 8.4375 13.394531 9.613281 12.625 10.96875 C 12.242188 11.648438 11.863281 12.386719 11.5625 13.0625 C 11.261719 13.738281 11 14.292969 11 15 L 11 20 C 11 21.644531 12.355469 23 14 23 L 19 23 C 19.824219 23 20.554688 22.585938 21 22.0625 C 21.445313 21.539063 21.707031 20.945313 21.9375 20.375 C 22.253906 19.585938 22.457031 18.378906 22.65625 17.25 C 22.855469 16.121094 23 15.15625 23 15.15625 L 23 15 C 23 13.355469 21.644531 12 20 12 L 16.375 12 L 16.9375 10.3125 L 16.90625 10.3125 C 17.070313 9.890625 17.152344 9.359375 16.9375 8.9375 C 16.710938 8.488281 16.320313 8.300781 16.03125 8.1875 C 15.457031 7.964844 14.96875 8 14.96875 8 Z M 14.1875 12.34375 L 14.0625 12.6875 L 13.625 14 L 20 14 C 20.539063 14 20.945313 14.433594 20.96875 14.96875 C 20.96875 14.96875 20.96875 14.996094 20.96875 15 L 19 15 L 19 17 L 20.65625 17 C 20.597656 17.332031 20.539063 17.667969 20.46875 18 L 19 18 L 19 20 L 19.90625 20 C 19.765625 20.328125 19.625 20.605469 19.5 20.75 C 19.328125 20.953125 19.277344 21 19 21 L 14 21 C 13.445313 21 13 20.554688 13 20 L 13 15 C 13 15 13.113281 14.457031 13.375 13.875 C 13.585938 13.40625 13.898438 12.871094 14.1875 12.34375 Z "></path>
                                                </g>
                                            </svg>
                                        </div>
                                    @else
                                        <div class="svg-container">
                                            <svg width="35" height="35" fill="#90A4AE" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1">
                                                <g id="surface1">
                                                    <path style=" " d="M 16 4 C 9.382813 4 4 9.382813 4 16 C 4 22.617188 9.382813 28 16 28 C 22.617188 28 28 22.617188 28 16 C 28 9.382813 22.617188 4 16 4 Z M 16 6 C 21.535156 6 26 10.464844 26 16 C 26 21.535156 21.535156 26 16 26 C 10.464844 26 6 21.535156 6 16 C 6 10.464844 10.464844 6 16 6 Z M 14 9 C 12.355469 9 11 10.355469 11 12 L 11 17 C 11 17.707031 11.261719 18.261719 11.5625 18.9375 C 11.863281 19.613281 12.242188 20.351563 12.625 21.03125 C 13.394531 22.386719 14.15625 23.5625 14.15625 23.5625 L 14.4375 24 L 14.96875 24 C 14.96875 24 15.457031 24.035156 16.03125 23.8125 C 16.320313 23.699219 16.710938 23.511719 16.9375 23.0625 C 17.152344 22.640625 17.070313 22.109375 16.90625 21.6875 L 16.9375 21.6875 L 16.375 20 L 20 20 C 21.644531 20 23 18.644531 23 17 L 23 16.84375 C 23 16.84375 22.855469 15.878906 22.65625 14.75 C 22.457031 13.621094 22.253906 12.414063 21.9375 11.625 C 21.707031 11.054688 21.445313 10.460938 21 9.9375 C 20.554688 9.414063 19.824219 9 19 9 Z M 14 11 L 19 11 C 19.277344 11 19.328125 11.046875 19.5 11.25 C 19.625 11.394531 19.765625 11.671875 19.90625 12 L 19 12 L 19 14 L 20.46875 14 C 20.539063 14.332031 20.597656 14.667969 20.65625 15 L 19 15 L 19 17 L 20.96875 17 C 20.96875 17.003906 20.96875 17.03125 20.96875 17.03125 C 20.945313 17.566406 20.539063 18 20 18 L 13.625 18 L 14.0625 19.3125 L 14.1875 19.65625 C 13.898438 19.128906 13.585938 18.59375 13.375 18.125 C 13.113281 17.542969 13 17 13 17 L 13 12 C 13 11.445313 13.445313 11 14 11 Z "></path>
                                                </g>
                                            </svg>
                                        </div>
                                    @endif

                                    <div>
                                        {{ $comment->phrase }}
                                        <i class="fa fa-quote-right"></i>
                                        <span class="comment-date">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $comment->store_date)->diffForHumans() }}</span>
                                    </div>
                                </div>

                            </li>
                        @endforeach
                    </ul>
                    {{--
                    <p class="text-muted mb-0">
                        <small>
                            The comments above (which are extracted and analyzed from the respective official app store) are the sole responsibility of their writers and the writers will take full responsibility, liability, and blame for any libel or litigation that result from something written in or as a direct result of something written in a comment.
                        </small>
                    </p>
                    --}}
                </div>
            </div>
        </div>
    </div>

@endsection