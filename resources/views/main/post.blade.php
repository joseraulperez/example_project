@extends('layouts.main')

@section('body_class'){{ 'post' }}@endsection
@section('meta_title'){{ $post->meta_title . ' | Zoolity' }}@endsection
@section('meta_description'){{ $post->meta_description }}@endsection
@section('content')

    <div class="container mt-4 mb-5">

        <div class="row">
            <div class="post col-lg-9">
                <h1 class="title mb-4">{{ $post->title }}</h1>
                <div class="lead text-muted mb-4">{{ $post->excerpt }}</div>

                <div class="post-featured-image mb-4">
                    <img src="{{ $post->featured_image }}" title="{{ $post->title }}">

                    @if (!empty($post->featured_image_source_name) && !empty($post->featured_image_source_link))
                        <p class="credits text-right">Source: <a rel="nofollow" target="_blank" href="{{ $post->featured_image_source_link }}">{{ $post->featured_image_source_name }}</a></p>
                    @endif
                </div>

                <div class="row d-flex justify-content-end">
                    <div class="col-lg-2">
                        <div class="post-author">
                            <img class="author-avatar" src="{{ asset('/img/zoolity-avatar.jpg') }}">
                            <span>by Zoolity</span>
                        </div>
                        <div class="post-share">
                            <p class="lead">Share</p>
                            <div class="share-box" id="share">
                                <!-- facebook -->
                                <a class="facebook" href="https://www.facebook.com/share.php?u={{ url()->current() }}&title={{ $post->title }}" target="blank"><i class="fa fa-facebook"></i></a>

                                <!-- twitter -->
                                <a class="twitter" href="https://twitter.com/intent/tweet?status={{ $post->title }}+{{ url()->current() }}" target="blank"><i class="fa fa-twitter"></i></a>

                                <!-- google plus -->
                                <a class="googleplus" href="https://plus.google.com/share?url={{ url()->current() }}" target="blank"><i class="fa fa-google-plus"></i></a>

                                <!-- pinterest -->
                                <a class="pinterest" href="https://pinterest.com/pin/create/bookmarklet/?media={{ $post->featured_image }}&url={{ url()->current() }}&is_video=false&description={{ $post->title }}" target="blank"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="post-content">
                            {!! $post->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @if(0 !== $posts->count())
    <div class="container-grey-wrapper pt-5 pb-5">
        <div class="container">
            <h3 class="mb-5">Related Articles</h3>
            <div class="row mt-4">
                @foreach($posts as $post)
                    <div class="col-lg-4 mb-4">
                        @include('main.partials.card_post')
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    <div class="container pt-5">
        <h3 class="mb-5">Latest Stories</h3>
        <div class="row">
            <div class="col-lg-9">
                @foreach($latest_posts as $post)
                    @include('main.partials.category_post')
                @endforeach
            </div>
        </div>
    </div>

    <script>
        window.onscroll = function() {myFunction()};

        // Get the navbar
        var share = $(".post-share");

        // Get the offset position of the navbar
        var sticky = share.offset().top - 25;

        // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
        function myFunction() {
            if (window.pageYOffset >= sticky) {
                share[0].classList.add("sticky")
            } else {
                share[0].classList.remove("sticky");
            }
        }
    </script>

@endsection