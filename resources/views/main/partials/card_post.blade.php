<a class="card card-post" href="{{ route('main_post', [$post->title_slug]) }}">
    <img class="card-img" src="{{ $post->featured_image }}" alt="{{ $post->title }}">
    <div class="card-body">
        <p class="title">{{ $post->title }}</p>
        <p class="text-muted excerpt">{{ $post->excerpt }}</p>
    </div>
</a>