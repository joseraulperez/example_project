<a class="category-post row mb-4" href="{{ route('main_post', [$post->title_slug]) }}">
    <div class="col-md-6">
        <img class="card-img" src="{{ $post->featured_image }}" alt="{{ $post->title }}">
    </div>
    <div class="col-md-6">
        <p class="title">{{ $post->title }}</p>
        <p class="text-muted excerpt">{{ $post->excerpt }}</p>
    </div>
</a>