<a class="card card-gallery" href="{{ route('main_gallery', [trans('route.gallery'), $gallery->title_slug]) }}">
    <img class="card-img" src="{{ $gallery->featured_image->url }}" alt="{{ $gallery->title }}">
    <div class="card-img-overlay d-flex justify-content-center align-items-center">
        <div class="text-center">
            <i class="fa fa-camera"></i>
            <p class="title">{{ $gallery->title }}</p>
        </div>
    </div>
</a>