<nav class="navbar-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <a class="navbar-brand" href="{{ route('main_home') }}">
                    @include('main.svg.logo')
                </a>
            </div>

            <div class="col-lg-10">
                {{--
                <form class="form" method="GET" action="{{ route('main_search_get') }}" >
                    <div class="input-group">
                        <input name="query" class="form-control" type="text" placeholder="Search" aria-label="Search" value="@if(isset($query)){{ $query }}@endif">
                        <button class="btn input-group-addon" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
                --}}
            </div>
        </div>
    </div>
</nav>