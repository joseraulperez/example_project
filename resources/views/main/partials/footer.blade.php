<footer class="pt-5 mt-5 pb-5 text-center">
    <div class="container">
        <div>
            <a href="{{ route('main_home') }}">{{ trans('footer.home') }}</a>
            ·
            <a rel="nofollow" href="{{ route('main_contact_us') }}">{{ trans('footer.contact_us') }}</a>
            ·
            <a rel="nofollow" href="{{ route('main_privacy_policy') }}">{{ trans('footer.privacy_policy') }}</a>
            ·
            <a rel="nofollow" href="{{ route('main_terms_of_service') }}">{{ trans('footer.terms_of_service') }}</a>
            ·
            © {{ date('Y') }} Zoolity
        </div>

        <div>
            <small>
                {{ trans('footer.other_languages') }}
                @foreach($locales as $locale => $name)
                    @if(app()->getLocale() !== $locale)
                        <a class="mr-1 ml-1" href="{{ config('urls.locale.'.$locale) }}">{{ $name }}</a>
                    @endif
                @endforeach
            </small>
        </div>
    </div>
</footer>