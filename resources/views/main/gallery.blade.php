@extends('layouts.main')

@section('body_class'){{ 'post' }}@endsection
@section('meta_title'){{ $gallery->meta_title . ' | Zoolity' }}@endsection
@section('meta_description'){{ $gallery->meta_description }}@endsection
@section('content')

    <div class="gallery-container pt-4 pb-5">
        <div class="container mb-5">
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="title mb-4">{{ $gallery->title }}</h1>
                    <a class="card card-gallery mb-4" href="{{ route('main_photo', [trans('route.gallery'), $gallery->title_slug, $first_photo->title_slug]) }}">
                        <img class="card-img" src="{{ $gallery->featured_image->url }}" alt="{{ $gallery->title }}">
                        <div class="card-img-overlay d-flex justify-content-center align-items-center">
                            <div class="text-center">
                                <i class="gallery-big-icon fa fa-play-circle"></i>
                            </div>
                        </div>
                    </a>
                    @if($gallery->excerpt)
                        <p class="description mb-0">{{ $gallery->excerpt }}</p>
                    @endif
                </div>
            </div>

        </div>
    </div>

@endsection