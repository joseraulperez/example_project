@extends('layouts.main')

@section('body_class'){{'home'}}@endsection
@section('meta_title'){{ $query . ' | Zoolity' }}@endsection
@section('meta_description'){{''}}@endsection
@section('content')

    <div class="container mt-3">

        <h1>{{ $query }}</h1>

        <div class="row">
            @foreach($posts as $post)
                <div class="col-md-6 mb-4">
                    @include('main.partials.card_post')
                </div>
            @endforeach
        </div>
    </div>

@endsection