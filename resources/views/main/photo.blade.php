@extends('layouts.main')

@section('body_class'){{ 'post' }}@endsection
@section('meta_title'){{ $photo->meta_title . ' | Zoolity' }}@endsection
@section('meta_description'){{ $photo->meta_description }}@endsection
@section('content')

    <div class="gallery-container pt-4 pb-5">
        <div class="container mb-5">
            <div class="row">
                <div class="gallery col-lg-9">
                    <h1 class="title mb-4">{{ $photo->title }}</h1>

                    <div class="gallery-image-container">

                        <img src="{{ $photo->url }}" title="{{ $photo->title }}" alt="{{ $gallery->title }}">

                        @if ($previous_photo)
                            <a class="carousel-control-prev" href="{{ route('main_photo', [trans('route.gallery'), $gallery->title_slug, $previous_photo->title_slug]) }}" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                        @endif

                        @if ($next_photo)
                            <a class="carousel-control-next" href="{{ route('main_photo', [trans('route.gallery'), $gallery->title_slug, $next_photo->title_slug]) }}" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        @endif
                    </div>

                    <p class="description mb-0 mt-4">{{ $photo->description }}</p>


                </div>

            </div>

        </div>
    </div>

@endsection