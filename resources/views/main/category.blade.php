@extends('layouts.main')

@section('body_class'){{ '' }}@endsection
@section('meta_title'){{ 'Articles about ' . $category->name . ' | Zoolity' }}@endsection
@section('meta_description'){{ 'Find the best articles about ' . $category->name . '| Zoolity' }}@endsection
@section('content')

    <div class="container mt-3">

        <h1 class="mb-3">{{ $category->name }}</h1>

        <div class="row">
            @foreach($posts as $post)
                <div class="col-md-6 mb-4">
                    @include('main.partials.card_post')
                </div>
            @endforeach
        </div>
    </div>
@endsection