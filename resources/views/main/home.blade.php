@extends('layouts.main')

@section('body_class'){{'home'}}@endsection
@section('meta_title'){{ trans('metas.home_title') }}@endsection
@section('meta_description'){{ trans('metas.home_description') }}@endsection
@section('content')
<div class="container mt-4">
    <div class="row">
        <div class="col-lg-8 mb-4">
            <a class="card card-post" href="{{ route('main_post', [$posts[0]->title_slug]) }}">
                <img class="card-img" src="{{ $posts[0]->featured_image }}" alt="{{ $posts[0]->title }}">
                <div class="card-img-overlay">
                    <p class="title">{{ $posts[0]->title }}</p>
                    <p class="excerpt">{{ $posts[0]->excerpt }}</p>
                </div>
            </a>
        </div>
        @foreach($posts->slice(1, 4) as $post)
            <div class="col-lg-4 mb-4">
                @include('main.partials.card_post')
            </div>
        @endforeach
    </div>
    <div class="row">
        @foreach($galleries as $gallery)
            <div class="col-lg-6 mb-4">
                @include('main.partials.card_gallery')
            </div>
        @endforeach
    </div>
    <div class="row">
        @foreach($posts->slice(5, 11) as $post)
            <div class="col-lg-4 mb-4">
                @include('main.partials.card_post')
            </div>
        @endforeach
    </div>
</div>
@endsection