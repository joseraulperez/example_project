<?php

namespace App\Http\Middleware;

use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $host = parse_url(url()->current())['host'];
        $sub_domain = explode('.', $host)[0];
        $sub_domain_data = config('sub_domains');

        if (isset($sub_domain_data['available'][$sub_domain])) {
            $locale = $sub_domain;
        } else {
            $locale = $sub_domain_data['default'];
        }

        app()->setLocale($locale);

        return $next($request);
    }
}
