<?php

namespace App\Http\Middleware;

use App\Models\Category;
use App\Models\Platform;
use Closure;

class CommonViewVariables
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share('locales', config('locales'));

        return $next($request);
    }
}
