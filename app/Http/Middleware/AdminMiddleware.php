<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(null !== Auth::user() && Auth::user()->role == \App\Models\User::ROLE_ADMIN)
        {
            return $next($request); // pass the admin
        }

        return app()->abort(404);

    }
}
