<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Photo;

class GalleryController extends Controller
{
    public function index(Gallery $gallery_model, $gallery_route_slug, $title_slug)
    {
        $locale = app()->getLocale();
        $gallery = $gallery_model->getGalleryByTitleSlug($locale, $title_slug);

        return view('main.gallery', [
            'gallery' => $gallery,
            'first_photo' => $this->getFirstPhoto($gallery->photos),
        ]);
    }

    private function getFirstPhoto($photos)
    {
        return $photos->where('position', 1)->first();
    }
}
