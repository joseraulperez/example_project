<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Post;

class PostController extends Controller
{
    public function index(Post $post_model, $title_slug)
    {
        $locale = app()->getLocale();
        $post = $post_model->getPostByTitleSlug($locale, $title_slug);
        $related_posts = $post_model->getRelatedPosts($locale, $post->category->id, $post->id);
        $latest_posts = $post_model->getPostsByLocale($locale, 5);

        return view('main.post', [
            'post' => $post,
            'posts' => $related_posts,
            'latest_posts' => $latest_posts,
        ]);
    }
}
