<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\App;
use App\Spiders\GooglePlayStore;
use App\Api\GoogleLanguage;

class StaticController extends Controller
{
    public function privacyPolicy()
    {
        return view('main.static.privacy_policy');
    }

    public function termsOfService()
    {
        return view('main.static.terms_of_service');
    }

    public function contactUs()
    {
        return view('main.static.contact_us');
    }
}
