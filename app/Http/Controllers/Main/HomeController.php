<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Post;

class HomeController extends Controller
{
    public function index(Post $post_model, Category $category_model, Gallery $gallery_model)
    {
        $locale = app()->getLocale();

        $posts = $post_model->getPostsByLocale($locale, 11);
        $categories = $category_model->getCategoriesByLocale($locale);
        $galleries = $gallery_model->getGalleriesByLocale($locale, 2);

        return view('main.home', [
            'posts' => $posts,
            'categories' => $categories,
            'galleries' => $galleries,
        ]);
    }
}
