<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Photo;

class PhotoController extends Controller
{
    public function index(Gallery $gallery_model, Photo $photo_model, $gallery_slug, $title_slug, $photo_slug = null)
    {
        $locale = app()->getLocale();
        $gallery = $gallery_model->getGalleryByTitleSlug($locale, $title_slug);
        $photo = $this->getPhoto($gallery->photos, $photo_slug);

        return view('main.photo', [
            'gallery' => $gallery,
            'photo' => $this->getPhoto($gallery->photos, $photo_slug),
            'next_photo' => $this->getNextPhoto($gallery->photos, $photo),
            'previous_photo' => $this->getPreviousPhoto($gallery->photos, $photo),
        ]);
    }

    private function getNextPhoto($photos, $photo)
    {
        if ($photos->count() === $photo->position) {
            return null;
        }
        return $photos->where('position', ($photo->position + 1))->first();
    }

    private function getPreviousPhoto($photos, $photo)
    {
        if (1 === $photo->position) {
            return null;
        }
        return $photos->where('position', ($photo->position - 1))->first();
    }

    private function getPhoto($photos, $photo_slug)
    {
        if (is_null($photo_slug)) {
            return $photos->where('is_featured', 1)->first();
        }

        return $photos->where('title_slug', $photo_slug)->first();
    }
}
