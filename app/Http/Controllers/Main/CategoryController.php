<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;

class CategoryController extends Controller
{
    public function index(Post $post_model, Category $category_model, $category_slug)
    {
        $locale = app()->getLocale();
        $category = $category_model->getCategoryBySlug($locale, $category_slug);
        $posts = $post_model->getPostsByCategory($locale, $category->id);

        return view('main.category', [
            'posts' => $posts,
            'category' => $category,
        ]);
    }
}
