<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\Photo;

class PhotosController extends Controller
{
    public function index(Gallery $gallery, Photo $photo, $gallery_id)
    {
        $gallery = $gallery
            ->where('id', $gallery_id)
            ->first();

        $photos = $photo->where('gallery_id', $gallery->id)
            ->get();

        return view('admin.photos.index', [
            'gallery' => $gallery,
            'photos' => $photos,
        ]);
    }

    public function create(Gallery $gallery, $gallery_id)
    {
        $gallery = $gallery
            ->where('id', $gallery_id)
            ->first();

        return view('admin.photos.create', [
            'gallery' => $gallery,
            'locales' => config('locales'),
        ]);
    }

    public function store(Photo $photo, $gallery_id)
    {
        $data = request()->all();

        $data['visible'] = isset($data['visible']) ? $data['visible'] : 0;
        $data['is_featured'] = isset($data['is_featured']) ? $data['is_featured'] : 0;
        $data['gallery_id'] = $gallery_id;

        $photo->create($data);

        return redirect()
            ->route('admin_photos', [$gallery_id])
            ->with('message', [
                'level' => 'success',
                'text' => 'Photo Created Successfully',
            ]);
    }

    public function edit(Gallery $gallery, Photo $photo, $gallery_id, $photo_id)
    {
        return view('admin.photos.edit', [
            'gallery' => $gallery->findOrFail($gallery_id),
            'photo' => $photo->findOrFail($photo_id),
        ]);
    }

    public function update(Photo $photo, $gallery_id, $photo_id)
    {
        $photo = $photo->find($photo_id);
        $data = request()->all();

        $data['visible'] = isset($data['visible']) ? $data['visible'] : 0;
        $data['is_featured'] = isset($data['is_featured']) ? $data['is_featured'] : 0;

        $photo->fill($data);
        $photo->save();

        return redirect()
            ->route('admin_photos', [$gallery_id])
            ->with('message', [
                'level' => 'success',
                'text' => 'Photo Updated Successfully',
            ]);
    }

    public function delete(Gallery $gallery, $id)
    {
        $gallery = $gallery->where('id', '=', $id)
            ->with('photos')
            ->first();

        if (0 < $gallery->photos->count()) {
            return redirect()
                ->route('admin_galleries')
                ->with('message', [
                    'level' => 'danger',
                    'text' => 'You have some photos in this gallery. Change them first before removing the category.',
                ]);
        }

        $gallery->delete();

        return redirect()
            ->route('admin_photos', [])
            ->with('message', [
                'level' => 'success',
                'text' => 'Gallery Removed Successfully',
            ]);
    }
}
