<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Link;

class LinksController extends Controller
{
    public function index(Link $link)
    {
        $links = $link
            ->orderByDesc('created_at')
            ->get();

        return view('admin.links.index', [
            'links' => $links,
        ]);
    }

    public function create()
    {
        return view('admin.links.create', []);
    }

    public function store(Link $link)
    {
        $link->create(request()->all());

        return redirect()
            ->route('admin_links')
            ->with('message', [
                'level' => 'success',
                'text' => 'Link Created Successfully',
            ]);
    }

    public function edit(Link $link, $id)
    {
        return view('admin.links.edit', [
            'link' => $link->findOrFail($id),
        ]);
    }

    public function update(Link $link, $id)
    {
        $link = $link->findOrFail($id);
        $data = request()->only(['original_link', 'affiliation_link', 'is_active']);
        $data['is_active'] = isset($data['is_active']) ? $data['is_active'] : 0;
        $link->fill($data);
        $link->save();
        return redirect()
            ->route('admin_links')
            ->with('message', [
                'level' => 'success',
                'text' => 'Link Updated Successfully',
            ]);
    }

    public function delete(Link $link, $id)
    {
        $link = $link->where('id', '=', $id)
            ->first();

        $link->delete();

        return redirect()
            ->route('admin_links')
            ->with('message', [
                'level' => 'success',
                'text' => 'Link Removed Successfully',
            ]);
    }
}
