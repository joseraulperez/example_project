<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Http\Controllers\Controller;
use App\Models\Category;

class CreateController extends Controller
{
    /**
     * Show the form and populate the categories (to choose a parent category).
     *
     * @param Category $category_model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Category $category_model)
    {
        // Prepare for HTML Select element.
        // TODO: categories should take into account the locale.
        $categories = $category_model
            ->all()
            ->pluck('name', 'id');

        return view('admin.categories.create', [
            'categories' => $categories,
            'locales' => config('locales'),
        ]);
    }

    /**
     * Validate & store a category.
     *
     * @param Category $category_model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Category $category_model)
    {
        $this->validate(request(), config('forms.rules.category'));
        $data = request()->all();

        try {
            $category_model->create($data);
        } catch(\Exception $e) {
            return redirect()
                ->back()
                ->withInput($data)
                ->with('message', [
                    'level' => 'danger',
                    'text' => $e->getMessage(),
                ]);
        }

        return redirect()
            ->route('admin_categories')
            ->with('message', [
                'level' => 'success',
                'text' => 'Category Created Successfully',
            ]);
    }
}
