<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Http\Controllers\Controller;
use App\Models\Category;

class IndexController extends Controller
{
    /**
     * Show categories.
     *
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Category $category)
    {
        $categories = $category
            ->orderByDesc('created_at')
            ->get();

        return view('admin.categories.index', [
            'categories' => $categories,
        ]);
    }
}
