<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Http\Controllers\Controller;
use App\Models\Category;

class EditController extends Controller
{
    /**
     * Show the form and populate the category and a list of categories (to choose a parent category).
     *
     * @param Category $category_model
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category_model, $id)
    {
        // Prepare for HTML Select element.
        // TODO: categories should take into account the locale.
        $categories = $category_model
            ->all()
            ->pluck('name', 'id');

        $category = $category_model
            ->findOrFail($id);

        return view('admin.categories.edit', [
            'category' => $category,
            'categories' => $categories,
            'locales' => config('locales'),
        ]);
    }

    /**
     * Store a category.
     *
     * @param Category $category
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Category $category, $id)
    {
        $this->validate(request(), config('forms.rules.category'));

        $category = $category->findOrFail($id);

        $data = request()->all();
        $category->fill($data);

        try {
            $category->save();
        } catch(\Exception $e) {
            return redirect()
                ->back()
                ->withInput($data)
                ->with('message', [
                    'level' => 'danger',
                    'text' => $e->getMessage(),
                ]);
        }

        return redirect()
            ->route('admin_categories')
            ->with('message', [
                'level' => 'success',
                'text' => 'Category Updated Successfully',
            ]);
    }
}
