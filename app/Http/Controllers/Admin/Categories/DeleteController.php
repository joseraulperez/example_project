<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Http\Controllers\Controller;
use App\Models\Category;

class DeleteController extends Controller
{
    /**
     * Delete a category.
     *
     * @param Category $category
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Category $category, $id)
    {
        $category = $category->where('id', '=', $id)
            ->with('posts')
            ->first();

        if (0 < $category->posts->count()) {
            return redirect()
                ->route('admin_categories')
                ->with('message', [
                    'level' => 'danger',
                    'text' => 'You have some posts with this category. Change them first before removing the category.',
                ]);
        }

        try {
            $category->delete();
        } catch(\Exception $e) {
            return redirect()
                ->back()
                ->with('message', [
                    'level' => 'danger',
                    'text' => $e->getMessage(),
                ]);
        }


        return redirect()
            ->route('admin_categories')
            ->with('message', [
                'level' => 'success',
                'text' => 'Category Removed Successfully',
            ]);
    }
}
