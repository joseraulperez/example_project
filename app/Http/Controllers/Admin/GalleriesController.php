<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;

class GalleriesController extends Controller
{
    public function index(Gallery $gallery)
    {
        $galleries = $gallery
            ->orderByDesc('created_at')
            ->get();

        return view('admin.galleries.index', [
            'galleries' => $galleries,
        ]);
    }

    public function create()
    {
        return view('admin.galleries.create', [
            'locales' => config('locales'),
        ]);
    }

    public function store(Gallery $gallery)
    {
        $data = request()->all();
        $data['visible'] = isset($data['visible']) ? $data['visible'] : 0;
        $gallery->fill($data);

        $gallery->create($data);

        return redirect()
            ->route('admin_galleries')
            ->with('message', [
                'level' => 'success',
                'text' => 'Gallery Created Successfully',
            ]);
    }

    public function edit(Gallery $gallery, $id)
    {
        return view('admin.galleries.edit', [
            'gallery' => $gallery->findOrFail($id),
            'locales' => config('locales'),
        ]);
    }

    public function update(Gallery $gallery, $id)
    {
        $gallery = $gallery->findOrFail($id);

        $data = request()->all();
        $data['visible'] = isset($data['visible']) ? $data['visible'] : 0;
        $gallery->fill($data);

        $gallery->save();
        return redirect()
            ->route('admin_galleries')
            ->with('message', [
                'level' => 'success',
                'text' => 'Gallery Updated Successfully',
            ]);
    }

    public function delete(Gallery $gallery, $id)
    {
        $gallery = $gallery->where('id', '=', $id)
            ->with('photos')
            ->first();

        if (0 < $gallery->photos->count()) {
            return redirect()
                ->route('admin_galleries')
                ->with('message', [
                    'level' => 'danger',
                    'text' => 'You have some photos in this gallery. Change them first before removing the category.',
                ]);
        }

        $gallery->delete();

        return redirect()
            ->route('admin_galleries')
            ->with('message', [
                'level' => 'success',
                'text' => 'Gallery Removed Successfully',
            ]);
    }
}
