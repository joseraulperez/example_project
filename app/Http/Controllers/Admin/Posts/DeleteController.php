<?php

namespace App\Http\Controllers\Admin\Posts;

use App\Http\Controllers\Controller;
use App\Models\Post;

class DeleteController extends Controller
{
    /**
     * Delete a post.
     *
     * @param  Post $post_model
     * @param  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Post $post_model, $id)
    {
        try {
            $post_model
                ->where('id', '=', $id)
                ->delete();
        } catch(\Exception $e) {
            return redirect()
                ->back()
                ->with('message', [
                    'level' => 'danger',
                    'text' => $e->getMessage(),
                ]);
        }

        return redirect()
            ->route('admin_posts')
            ->with('message', [
                'level' => 'success',
                'text' => 'Post Removed Successfully',
            ]);
    }
}
