<?php

namespace App\Http\Controllers\Admin\Posts;

use App\Http\Controllers\Controller;
use App\Models\Post;

class IndexController extends Controller
{
    /**
     * Show posts.
     *
     * @param  Post $post_model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Post $post_model)
    {
        $posts = $post_model
            ->orderByDesc('created_at')
            ->get();

        return view('admin.posts.index', [
            'posts' => $posts,
        ]);
    }
}
