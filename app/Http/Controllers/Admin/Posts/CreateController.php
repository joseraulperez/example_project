<?php

namespace App\Http\Controllers\Admin\Posts;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Traits\Admin\FilterFormPost;

class CreateController extends Controller
{
    use FilterFormPost;

    /**
     * Show the form and populate the categories.
     *
     * @param Category $category_model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Category $category_model)
    {
        // Prepare for HTML Select element.
        $categories = $category_model
            ->all()
            ->pluck('name', 'id');

        return view('admin.posts.create', [
            'categories' => $categories,
            'locales' => config('locales'),
        ]);
    }

    /**
     * Store a post.
     *
     * @param Post $post_model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Post $post_model)
    {
        $this->validate(request(), config('forms.rules.post'));

        $data = request()->all();
        $data = $this->filterForm($data);

        try {
            $post_model->create($data);
        } catch(\Exception $e) {
            return redirect()
                ->back()
                ->withInput($data)
                ->with('message', [
                    'level' => 'danger',
                    'text' => $e->getMessage(),
                ]);
        }

        return redirect()
            ->route('admin_posts')
            ->with('message', [
                'level' => 'success',
                'text' => 'Post Created Successfully',
            ]);
    }
}
