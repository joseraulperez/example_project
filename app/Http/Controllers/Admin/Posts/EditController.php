<?php

namespace App\Http\Controllers\Admin\Posts;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Traits\Admin\FilterFormPost;

class EditController extends Controller
{
    use FilterFormPost;

    /**
     * Show the form and populate categories, post & available locales.
     *
     * @param  Category $category_model
     * @param  Post $post_model
     * @param  integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category_model, Post $post_model, $id)
    {
        // Prepare for Select element.
        $categories = $category_model
            ->all()
            ->pluck('name', 'id');

        $post = $post_model->findOrFail($id);

        return view('admin.posts.edit', [
            'categories' => $categories,
            'post' => $post,
            'locales' => config('locales'),
        ]);
    }

    /**
     * Update the post.
     *
     * @param  Post $post_model
     * @param  integer $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Post $post_model, $id)
    {
        $this->validate(request(), config('forms.rules.post'));

        $post = $post_model->findOrFail($id);
        $data = request()->all();
        $data = $this->filterForm($data);

        $post->fill($data);

        try {
            $post->save();
        } catch(\Exception $e) {
            return redirect()
                ->back()
                ->withInput($data)
                ->with('message', [
                    'level' => 'danger',
                    'text' => $e->getMessage(),
                ]);
        }

        return redirect()
            ->route('admin_posts_edit', [$id])
            ->with('message', [
                'level' => 'success',
                'text' => 'Post Updated Successfully',
            ]);
    }
}
