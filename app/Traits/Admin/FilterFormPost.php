<?php

namespace App\Traits\Admin;

trait FilterFormPost
{
    /**
     * Filter the data that comes from the form.
     * Notice the problem with the boolean elements,
     * if they are "off", the key is not present, so won't be updated/stored.
     *
     * @param  array $data
     * @return array
     */
    public function filterForm(array $data)
    {
        $data['visible'] = isset($data['visible']) ? $data['visible'] : 0;

        return $data;
    }
}