<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $locale = $this->getLocaleFromUrl();

        $category_model = new Category();
        $category_slugs = $category_model->getCategoriesSlugForRouting($locale);

        // Url Patters. REMEMBER TO CACHE ALL!!
        Route::pattern('category_slug', $category_slugs);
        Route::pattern('gallery_slug', trans('route.gallery', [], $locale));

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }


    private function getLocaleFromUrl()
    {
        $host = parse_url(url()->current())['host'];
        $sub_domain = explode('.', $host)[0];

        if ('www' === $sub_domain) {
            $locale = 'en';
        } else {
            $locale = $sub_domain;
        }

        return $locale;
    }
}
