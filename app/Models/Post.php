<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = ['id'];

    /*
    |--------------------------------------------------------------------------
    | Queries
    |--------------------------------------------------------------------------
    */
    public function getPostsByLocale($locale, $limit = null)
    {
        $query = $this
            ->where('locale', $locale)
            ->where('visible', 1)
            ->orderByDesc('created_at');

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        return $query->get();
    }

    public function getPostsByCategory($locale, $category_id)
    {
        return $this
            ->where('locale', $locale)
            ->where('category_id', $category_id)
            ->where('visible', 1)
            ->orderByDesc('created_at')
            ->get();
    }

    public function getPostByTitleSlug($locale, $title_slug)
    {
        return $this
            ->where('locale', $locale)
            ->where('title_slug', $title_slug)
            ->firstOrFail();
    }

    public function getRelatedPosts($locale, $category_id, $post_id)
    {
        return $this
            ->where('locale', $locale)
            ->where('category_id', $category_id)
            ->where('id', '!=', $post_id)
            ->where('visible', 1)
            ->orderByDesc('created_at')
            ->get();
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}