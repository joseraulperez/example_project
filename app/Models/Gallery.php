<?php

namespace App\Models;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded = ['id'];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */
    public function photos()
    {
        return $this->hasMany('App\Models\Photo')
            ->orderBy('position');
    }

    public function featured_image()
    {
        return $this->hasOne('App\Models\Photo')
            ->where('is_featured', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | Queries
    |--------------------------------------------------------------------------
    */
    public function getGalleriesByLocale($locale, $limit = null)
    {
        $query = $this
            ->with('featured_image')
            ->where('visible', 1)
            ->where('locale', $locale);

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        return $query->orderBy('created_at')
            ->get();
    }

    public function getCategoriesSlugForRouting($locale)
    {
        if (Schema::hasTable('categories')) {

            $categories = $this
                ->where('locale', $locale)
                ->orderBy('name')
                ->get()
                ->pluck('name_slug')
                ->toArray();

            if (!empty($categories)) {
                return implode('|', $categories);
            }
            return 'none';
        }
        return 'none';
    }

    public function getGalleryByTitleSlug($locale, $gallery_slug)
    {
        return $this
//            ->with('photos')
            ->where('locale', $locale)
            ->where('title_slug', $gallery_slug)
            ->firstOrFail();
    }

}