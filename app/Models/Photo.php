<?php

namespace App\Models;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $guarded = ['id'];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */
    public function gallery()
    {
        return $this->belongsTo('App\Models\Gallery');
    }
}