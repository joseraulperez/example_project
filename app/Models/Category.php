<?php

namespace App\Models;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id'];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    /*
    |--------------------------------------------------------------------------
    | Queries
    |--------------------------------------------------------------------------
    */
    public function getCategoriesByLocale($locale)
    {
        return $this
            ->where('locale', $locale)
            ->orderBy('name')
            ->get();
    }

    public function getCategoriesSlugForRouting($locale)
    {
        if (Schema::hasTable('categories')) {

            $categories = $this
                ->where('locale', $locale)
                ->orderBy('name')
                ->get()
                ->pluck('name_slug')
                ->toArray();

            if (!empty($categories)) {
                return implode('|', $categories);
            }
            return 'none';
        }
        return 'none';
    }

    public function getCategoryBySlug($locale, $category_slug)
    {
        return $categories = $this
            ->where('locale', $locale)
            ->where('name_slug', $category_slug)
            ->firstOrFail();
    }

}