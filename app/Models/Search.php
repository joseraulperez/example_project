<?php

namespace App\Models;

use DB;

class Search {

    public function search($query, $locale, $items_per_page)
    {
        $query = $this->filterStopWords($query);

        $sql = <<<SQL
SELECT
  posts.id, 
  MATCH (posts.title, posts.excerpt) AGAINST (:query1) AS relevance
FROM posts
WHERE MATCH (posts.title, posts.excerpt) AGAINST (:query2)
AND posts.locale = :locale
ORDER BY relevance DESC
LIMIT $items_per_page
SQL;

        $data = DB::select($sql, [
            'query1' => $query,
            'query2' => $query,
            'locale' => $locale,
        ]);

        return $this->getPosts($data);
    }

    private function getPosts($data)
    {
        $ids = array_column($data, 'id');
        return Post::whereIn('id', $ids)->get();
    }

    private function filterStopWords($query)
    {
        $query = explode(' ', $query);
        $stop_words = $this->getStopWords();
        $query = array_diff($query, $stop_words);
        return implode(' ', $query);
    }

    private function getStopWords()
    {
        $stop_words = config('search.stop_words.'.app()->getLocale());

        if (is_null($stop_words)) {
            return config('search.stop_words.en');
        }

        return $stop_words;
    }
}