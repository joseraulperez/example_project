<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin', 'middleware'=>['admin']], function() {

    Route::get('/admin', [
        'uses'      => 'HomeController@index',
        'as'        => 'admin_home'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Techy
    |--------------------------------------------------------------------------
    */
    Route::get('/admin/techy/logs', [
        'uses'      => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index',
        'as'        => 'admin_techy_logs'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Categories
    |--------------------------------------------------------------------------
    */
    Route::group(['namespace' => 'Categories'], function() {

        Route::get('/admin/categories', [
            'uses'      => 'IndexController@index',
            'as'        => 'admin_categories'
        ]);

        // Create
        Route::get('/admin/categories/create', [
            'uses'      => 'CreateController@create',
            'as'        => 'admin_categories_create'
        ]);

        Route::post('/admin/categories/create', [
            'uses'      => 'CreateController@store'
        ]);

        // Edit
        Route::get('/admin/categories/{id}', [
            'uses'      => 'EditController@edit',
            'as'        => 'admin_categories_edit'
        ]);

        Route::post('/admin/categories/{id}', [
            'uses'      => 'EditController@update'
        ]);

        // Delete
        Route::get('/admin/categories/{id}/delete', [
            'uses'      => 'DeleteController@delete',
            'as'        => 'admin_categories_delete'
        ]);
    });

    /*
    |--------------------------------------------------------------------------
    | Galleries
    |--------------------------------------------------------------------------
    */
    Route::get('/admin/galleries', [
        'uses'      => 'Galleries\IndexController@index',
        'as'        => 'admin_galleries'
    ]);

    Route::get('/admin/galleries/create', [
        'uses'      => 'GalleriesController@create',
        'as'        => 'admin_galleries_create'
    ]);

    Route::post('/admin/galleries/create', [
        'uses'      => 'GalleriesController@store'
    ]);

    Route::get('/admin/galleries/{id}', [
        'uses'      => 'GalleriesController@edit',
        'as'        => 'admin_galleries_edit'
    ]);

    Route::post('/admin/galleries/{id}', [
        'uses'      => 'GalleriesController@update'
    ]);

    Route::get('/admin/galleries/{id}/delete', [
        'uses'      => 'GalleriesController@delete',
        'as'        => 'admin_galleries_delete'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Photos
    |--------------------------------------------------------------------------
    */
    Route::get('/admin/galleries/{gallery_id}/photos', [
        'uses'      => 'PhotosController@index',
        'as'        => 'admin_photos'
    ]);

    Route::get('/admin/galleries/{gallery_id}/photos/create', [
        'uses'      => 'PhotosController@create',
        'as'        => 'admin_photos_create'
    ]);

    Route::post('/admin/galleries/{gallery_id}/photos/create', [
        'uses'      => 'PhotosController@store'
    ]);

    Route::get('/admin/galleries/{gallery_id}/photos/{id}', [
        'uses'      => 'PhotosController@edit',
        'as'        => 'admin_photos_edit'
    ]);

    Route::post('/admin/galleries/{gallery_id}/photos/{id}', [
        'uses'      => 'PhotosController@update'
    ]);

    Route::get('/admin/galleries/{gallery_id}/photos/{id}/delete', [
        'uses'      => 'PhotosController@delete',
        'as'        => 'admin_photos_delete'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Links
    |--------------------------------------------------------------------------
    */
    Route::get('/admin/links', [
        'uses'      => 'LinksController@index',
        'as'        => 'admin_links'
    ]);

    Route::get('/admin/links/create', [
        'uses'      => 'LinksController@create',
        'as'        => 'admin_links_create'
    ]);

    Route::post('/admin/links/create', [
        'uses'      => 'LinksController@store'
    ]);

    Route::get('/admin/links/{id}', [
        'uses'      => 'LinksController@edit',
        'as'        => 'admin_links_edit'
    ]);

    Route::post('/admin/links/{id}', [
        'uses'      => 'LinksController@update'
    ]);

    Route::get('/admin/links/{id}/delete', [
        'uses'      => 'LinksController@delete',
        'as'        => 'admin_links_delete'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Posts
    |--------------------------------------------------------------------------
    */
    Route::get('/admin/posts', [
        'uses'      => 'Posts\IndexController@index',
        'as'        => 'admin_posts'
    ]);

    // Create
    Route::get('/admin/posts/create', [
        'uses'      => 'Posts\CreateController@create',
        'as'        => 'admin_posts_create'
    ]);
    Route::post('/admin/posts/create', [
        'uses'      => 'Posts\CreateController@store'
    ]);

    // Edit
    Route::get('/admin/posts/{id}', [
        'uses'      => 'Posts\EditController@edit',
        'as'        => 'admin_posts_edit'
    ]);
    Route::post('/admin/posts/{id}', [
        'uses'      => 'Posts\EditController@update'
    ]);

    // Delete
    Route::get('/admin/posts/{id}/delete', [
        'uses'      => 'Posts\DeleteController@delete',
        'as'        => 'admin_posts_delete'
    ]);

});


Route::group(['middleware'=> ['common_view_variables']], function() {

    /*
    |--------------------------------------------------------------------------
    | Auth
    |--------------------------------------------------------------------------
    */
    Auth::routes();

    /*
    |--------------------------------------------------------------------------
    | Main
    |--------------------------------------------------------------------------
    */
    Route::group(['namespace' => 'Main'], function() {

        /*
        |--------------------------------------------------------------------------
        | Home
        |--------------------------------------------------------------------------
        */
        Route::get('/', [
            'uses'      => 'HomeController@index',
            'as'        => 'main_home',
        ]);

        /*
        |--------------------------------------------------------------------------
        | Search
        |--------------------------------------------------------------------------
        */
        Route::get('/s/{query}', [
            'uses'      => 'SearchController@index',
            'as'        => 'main_search',
        ]);
        Route::get('/s', [
            'uses'      => 'SearchController@index',
            'as'        => 'main_search_get',
        ]);

        /*
        |--------------------------------------------------------------------------
        | Static
        |--------------------------------------------------------------------------
        */
        Route::get('/terms-of-service', [
            'uses'      => 'StaticController@termsOfService',
            'as'        => 'main_terms_of_service',
        ]);

        Route::get('/privacy-policy', [
            'uses'      => 'StaticController@privacyPolicy',
            'as'        => 'main_privacy_policy',
        ]);
        Route::get('/contact-us', [
            'uses'      => 'StaticController@contactUs',
            'as'        => 'main_contact_us',
        ]);

        /*
        |--------------------------------------------------------------------------
        | Category
        |--------------------------------------------------------------------------
        */
        Route::get('/{category_slug}', [
            'uses'      => 'CategoryController@index',
            'as'        => 'main_category',
        ]);

        /*
        |--------------------------------------------------------------------------
        | Post
        |--------------------------------------------------------------------------
        */
        Route::get('/{title_slug}', [
            'uses'      => 'PostController@index',
            'as'        => 'main_post',
        ]);

        /*
        |--------------------------------------------------------------------------
        | Gallery
        |--------------------------------------------------------------------------
        */
        Route::get('/{gallery_slug}/{title_slug}', [
            'uses'      => 'GalleryController@index',
            'as'        => 'main_gallery',
        ]);
        Route::get('/{gallery_slug}/{title_slug}/{photo_slug}', [
            'uses'      => 'PhotoController@index',
            'as'        => 'main_photo',
        ]);

    });
});