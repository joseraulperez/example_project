<?php

// php vendor/deployer/deployer/bin/dep deploy production

namespace Deployer;

require 'recipe/laravel.php';

// Configuration
set('repository', 'git@bitbucket.org:joseraulperez/zoolity.com.git');
set('git_tty', false); // [Optional] Allocate tty for git on first deployment

add('shared_files', ['.env']);
add('shared_dirs', ['storage']);
add('writable_dirs', []);


// Hosts

host('miraquemancha.com')
    ->stage('production')
    ->port(93)
    ->user('miraquem')
    ->forwardAgent(true)
    ->multiplexing(false)
    ->set('deploy_path', '~/webs/zoolity.com');

// Tasks

task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
//    'deploy:writable',
//    'deploy:writable',
    'deploy:symlink',
    'cleanup',
    'artisan:migrate',
    'artisan:cache:clear',
    'artisan:config:cache',
])->desc('Deploy your project');

after('deploy', 'success');


